@echo off
clink set history.shared true

clink set clink.logo none

clink set color.argmatcher bright green
clink set color.cmd green
clink set color.doskey green
clink set color.executable green
clink set color.unrecognized red
clink set color.input white

clink set color.cmdsep white
clink set color.cmdredir white

clink set color.flag cyan
clink set color.arg white
clink set color.suggestion bright black