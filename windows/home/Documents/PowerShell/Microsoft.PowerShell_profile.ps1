oh-my-posh init pwsh --config "~\.config\omp\lambda.omp.json" | Invoke-Expression

# Colors
Set-PSReadlineOption -Colors @{
  "InlinePrediction"="`e[90m"
  "Command"="`e[39m"
  "Parameter"="`e[95m"
  "Operator"="`e[95m"
  "String"="`e[93m"
  "Variable"="`e[94m"
}

# Binds
## Ctrl+f = Tab
Set-PSReadLineKeyHandler -Key 'Ctrl+f' -Function TabCompleteNext
Set-PSReadLineKeyHandler -Key 'Ctrl+g' -Function AcceptSuggestion

## Make up/down search history
Set-PSReadLineOption -HistorySearchCursorMovesToEnd
Set-PSReadLineKeyHandler -Key 'UpArrow' -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Key 'DownArrow' -Function HistorySearchForward

## Ctrl+d = Exit
Set-PSReadLineKeyHandler -Key 'Ctrl+d' -Function DeleteCharOrExit

# Aliases
Set-Alias neofetch pwshfetch-test-1
Set-Alias which Get-Command

# Environment
[Environment]::SetEnvironmentVariable("GIT_SSH", "$((Get-Command ssh).Source)", [System.EnvironmentVariableTarget]::User)
[Environment]::SetEnvironmentVariable("EDITOR", "nvim", [System.EnvironmentVariableTarget]::User)
[Environment]::SetEnvironmentVariable("BAT_THEME", "base16", [System.EnvironmentVariableTarget]::User)

# Modules
If (-not(Get-Module -ListAvailable -Name syntax-highlighting)) {
  Install-Module syntax-highlighting
}
Import-Module syntax-highlighting

If (-not(Get-Module -ListAvailable -Name syntax-highlighting)) {
  Install-Module ZLocation
}
Import-Module ZLocation
