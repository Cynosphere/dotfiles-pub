#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance force

~LWin::
    Send {Blind}{vkE8}
return

CenterWindow(WinTitle)
{
    WinGetPos,,, Width, Height, %WinTitle%
    WinMove, %WinTitle%,, (A_ScreenWidth/2)-(Width/2), (A_ScreenHeight/2)-(Height/2)
}

#+r::
    WinGetActiveTitle, Title
    WinMove, %Title%, , , ,1600,900
    CenterWindow(Title)
return
#+!r::
    WinGetActiveTitle, Title
    CenterWindow(Title)
return

#+LButton::
    WinGetActiveTitle, Title
    WinSet, Style, -0xC00000, %Title%
return
#+RButton::
    WinGetActiveTitle, Title
    WinSet, Style, +0xC00000, %Title%
return

#+MButton::
    WinSet, Transparent, 255, A
return
#+WheelUp::
    WinGet, Alpha, Transparent, A
    if (!Alpha)
        Alpha = 255

    Alpha += 4
    if (Alpha > 255)
        Alpha = 255

    WinSet, Transparent, %Alpha%, A
return
#+WheelDown::
    WinGet, Alpha, Transparent, A
    if (!Alpha)
        Alpha = 255

    Alpha -= 4
    if (Alpha < 56)
        Alpha = 56

    WinSet, Transparent, %Alpha%, A
return

holdm1 := 0
holdm2 := 0
#F12::
    holdm1 := !holdm1

    if (holdm1 = 0) {
        Click up
    } else {
        Click down
    }
return
#+F12::
    holdm2 := !holdm2

    if (holdm2 = 0) {
        Click up Right
    } else {
        Click down Right
    }
return

!l::
    Send {Right}
return
!k::
    Send {Up}
return
!j::
    Send {Down}
return
!h::
    Send {Left}
return

shockwaveRebind := 1
#IfWinActive ahk_exe SPR.exe
F12::
    shockwaveRebind := !shockwaveRebind
return
w::
    if (shockwaveRebind = 1) {
        Send {Up down}
    } else {
        Send {w down}
    }
return
w Up::
    if (shockwaveRebind = 1) {
        Send {Up up}
    } else {
        Send {w up}
    }
return
a::
    if (shockwaveRebind = 1) {
        Send {Left down}
    } else {
        Send {a down}
    }
return
a Up::
    if (shockwaveRebind = 1) {
        Send {Left up}
    } else {
        Send {a up}
    }
return
s::
    if (shockwaveRebind = 1) {
        Send {Down down}
    } else {
        Send {s down}
    }
return
s Up::
    if (shockwaveRebind = 1) {
        Send {Down up}
    } else {
        Send {s up}
    }
return
d::
    if (shockwaveRebind = 1) {
        Send {Right down}
    } else {
        Send {d down}
    }
return
d Up::
    if (shockwaveRebind = 1) {
        Send {Right up}
    } else {
        Send {d up}
    }
return
#IfWinActive

ShellRun(prms*)
{
    shellWindows := ComObjCreate("Shell.Application").Windows
    VarSetCapacity(_hwnd, 4, 0)
    desktop := shellWindows.FindWindowSW(0, "", 8, ComObj(0x4003, &_hwnd), 1)

    ; Retrieve top-level browser object.
    if ptlb := ComObjQuery(desktop
        , "{4C96BE40-915C-11CF-99D3-00AA004AE837}"  ; SID_STopLevelBrowser
        , "{000214E2-0000-0000-C000-000000000046}") ; IID_IShellBrowser
    {
        ; IShellBrowser.QueryActiveShellView -> IShellView
        if DllCall(NumGet(NumGet(ptlb+0)+15*A_PtrSize), "ptr", ptlb, "ptr*", psv:=0) = 0
        {
            ; Define IID_IDispatch.
            VarSetCapacity(IID_IDispatch, 16)
            NumPut(0x46000000000000C0, NumPut(0x20400, IID_IDispatch, "int64"), "int64")

            ; IShellView.GetItemObject -> IDispatch (object which implements IShellFolderViewDual)
            DllCall(NumGet(NumGet(psv+0)+15*A_PtrSize), "ptr", psv
                , "uint", 0, "ptr", &IID_IDispatch, "ptr*", pdisp:=0)

            ; Get Shell object.
            shell := ComObj(9,pdisp,1).Application

            ; IShellDispatch2.ShellExecute
            shell.ShellExecute(prms*)

            ObjRelease(psv)
        }
        ObjRelease(ptlb)
    }
}

#Enter::
    ShellRun("nircmd", "exec hide wezterm start")
return