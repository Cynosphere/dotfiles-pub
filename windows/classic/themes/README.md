# Classic Color Schemes

## Folder Differences
* `wctc` - Uses [WinClassicThemeConfig](https://gitlab.com/ftortoriello/WinClassicThemeConfig)
* `deskn` - Uses the stock Appearance modal (deskn.cpl or the desk2k.cpl mod)
* `theme` - .theme files exported from Windows XP. Works with [Chicago95 Plus!](https://github.com/grassmunk/Chicago95/tree/master/Plus)
  * `python ChicagoPlus.py --nocursors --noicons --nowallpaper --nosounds --nocolors --nofonts --noscreensaver file.theme`