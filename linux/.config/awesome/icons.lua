return function(path, theme)
    theme.menu_submenu_icon = path .. "/submenu.png"

    -- titlebar
    theme.titlebar_close_button_normal = path .. "/titlebar/close.png"
    theme.titlebar_close_button_focus  = path .. "/titlebar/close.png"

    theme.titlebar_minimize_button_normal = path .. "/titlebar/minimize.png"
    theme.titlebar_minimize_button_focus  = path .. "/titlebar/minimize.png"

    theme.titlebar_ontop_button_normal_inactive = path .. "/titlebar/ontop_inactive.png"
    theme.titlebar_ontop_button_focus_inactive  = path .. "/titlebar/ontop_inactive.png"
    theme.titlebar_ontop_button_normal_active = path .. "/titlebar/ontop_active.png"
    theme.titlebar_ontop_button_focus_active  = path .. "/titlebar/ontop_active.png"

    theme.titlebar_sticky_button_normal_inactive = path .. "/titlebar/sticky_inactive.png"
    theme.titlebar_sticky_button_focus_inactive  = path .. "/titlebar/sticky_inactive.png"
    theme.titlebar_sticky_button_normal_active = path .. "/titlebar/sticky_active.png"
    theme.titlebar_sticky_button_focus_active  = path .. "/titlebar/sticky_active.png"

    theme.titlebar_floating_button_normal_inactive = path .. "/titlebar/floating_inactive.png"
    theme.titlebar_floating_button_focus_inactive  = path .. "/titlebar/floating_inactive.png"
    theme.titlebar_floating_button_normal_active = path .. "/titlebar/floating_active.png"
    theme.titlebar_floating_button_focus_active  = path .. "/titlebar/floating_active.png"

    theme.titlebar_maximized_button_normal_inactive = path .. "/titlebar/maximized_inactive.png"
    theme.titlebar_maximized_button_focus_inactive  = path .. "/titlebar/maximized_inactive.png"
    theme.titlebar_maximized_button_normal_active = path .. "/titlebar/maximized_active.png"
    theme.titlebar_maximized_button_focus_active  = path .. "/titlebar/maximized_active.png"

    -- layouts
    theme.layout_fairh = path .. "/layouts/fairh.png"
    theme.layout_fairv = path .. "/layouts/fairv.png"
    theme.layout_floating  = path .. "/layouts/floating.png"
    theme.layout_magnifier = path .. "/layouts/magnifier.png"
    theme.layout_max = path .. "/layouts/max.png"
    theme.layout_fullscreen = path .. "/layouts/fullscreen.png"
    theme.layout_tilebottom = path .. "/layouts/tilebottom.png"
    theme.layout_tileleft   = path .. "/layouts/tileleft.png"
    theme.layout_tile = path .. "/layouts/tile.png"
    theme.layout_tiletop = path .. "/layouts/tiletop.png"
    theme.layout_spiral  = path .. "/layouts/spiral.png"
    theme.layout_dwindle = path .. "/layouts/dwindle.png"
    theme.layout_cornernw = path .. "/layouts/cornernw.png"
    theme.layout_cornerne = path .. "/layouts/cornerne.png"
    theme.layout_cornersw = path .. "/layouts/cornersw.png"
    theme.layout_cornerse = path .. "/layouts/cornerse.png"

    -- layouts (lain)
    theme.layout_termfair    = path .. "/layouts/lain/termfair.png"
    theme.layout_centerfair  = path .. "/layouts/lain/centerfair.png"
    theme.layout_cascade     = path .. "/layouts/lain/cascade.png"
    theme.layout_cascadetile = path .. "/layouts/lain/cascadetile.png"
    theme.layout_centerwork  = path .. "/layouts/lain/centerwork.png"
    theme.layout_centerworkh = path .. "/layouts/lain/centerworkh.png"
end
