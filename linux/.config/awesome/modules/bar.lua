-- luacheck: globals client
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local lain = require("lain")
local wibox = require("wibox")

local menu = require("modules/menu")
local utils = require("modules/utils")
local vars = require("modules/vars")
local win9x = require("modules/win9x")

local markup = lain.util.markup
local modkey = vars.modkey

beautiful.wibar_height = 28

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
  awful.button({}, 1, function(t)
    t:view_only()
  end),
  awful.button({modkey}, 1, function(t)
    if client.focus then
      client.focus:move_to_tag(t)
    end
  end),
  awful.button({}, 3, awful.tag.viewtoggle),
  awful.button({modkey}, 3, function(t)
    if client.focus then
      client.focus:toggle_tag(t)
    end
  end),
  awful.button({}, 4, function(t)
    awful.tag.viewnext(t.screen)
  end),
  awful.button({}, 5, function(t)
    awful.tag.viewprev(t.screen)
  end)
)

local tasklist_buttons = gears.table.join(
  awful.button({}, 1, function(c)
    if c == client.focus then
      c.minimized = true
    else
      c:emit_signal(
      "request::activate",
      "tasklist",
      {raise = true}
    )
  end
  end),
  awful.button({}, 3, function()
    awful.menu.client_list({
      theme = {
        width = 256
      }
    })
  end),
  awful.button({}, 4, function()
    awful.client.focus.byidx(1)
  end),
  awful.button({}, 5, function()
    awful.client.focus.byidx(-1)
  end)
)

local layout_buttons = gears.table.join(
  awful.button({}, 1, function()
    awful.layout.inc(1)
  end),
  awful.button({}, 3, function()
    awful.layout.inc(-1)
  end),
  awful.button({}, 4, function()
    awful.layout.inc(1)
  end),
  awful.button({}, 5, function()
    awful.layout.inc(-1)
  end)
)

--[[local function make_bar_icon(icon)
  local img = wibox.widget.imagebox(beautiful["bar_" .. icon], false)
  img.forced_height = 18
  img.forced_width = 18
  return {
    img,
    top = 1,
    layout = wibox.container.margin,
  }
end--]]
local function make_bar_icon(icon)
  local img = wibox.widget.imagebox(vars.env.ICON_DIR .. "/bar/9x/" .. icon .. ".png", false)
  img.forced_height = 16
  img.forced_width = 16
  return {
    img,
    top = 1,
    left = 2,
    right = 2,
    layout = wibox.container.margin,
  }
end

local function make_inset(child)
  return {
    {
      child,
      margins = 3,
      layout = wibox.container.margin,
    },
    shape = win9x.inset,
    layout = wibox.container.background,
  }
end

local function make_divider()
  return {
    {
      wibox.widget({}),
      forced_width = 3,
      shape = win9x.divider,
      layout = wibox.container.background,
    },
    left = 4,
    right = 4,
    top = 2,
    bottom = 2,
    layout = wibox.container.margin,
  }
end

local function status_box(child)
  return {
    make_divider(),
    {
      child,
      top = 2,
      bottom = 2,
      layout = wibox.container.margin,
    },
    layout = wibox.layout.fixed.horizontal,
  }
end

-- Widgets
--local clock_icon = make_bar_icon("clock")
local uclock = wibox.widget.textclock(markup(beautiful.widget_clock or beautiful.wibar_fg, "(UTC: %H) "), 1, "UTC")
local clock = wibox.widget.textclock(markup(beautiful.widget_clock or beautiful.wibar_fg, "%H:%M:%S"), 1)
lain.widget.cal({
  attach_to = {clock},
  week_start = 1,
  week_number = "left",
  followtag = true,
  icons = "",
  notification_preset = {
    font = vars.config.font,
    fg = beautiful.notification_fg,
    bg = beautiful.notification_bg,
  },
})

--[[local volume_icon = make_bar_icon("volume")
local volume = lain.widget.alsa({
  settings = function()
    if volume_now.status == "off" then
      volume_now.level = "MUTE"
    else
      volume_now.level = volume_now.level .. "%"
    end

    volume_icon.image = volume_now.status == "off" and beautiful.bar_volume_muted or beautiful.bar_volume

    widget:set_markup(markup(volume_now.status == "off" and beautiful.widget_volume_muted or beautiful.widget_volume, volume_now.level))
  end,
})
local volume_buttons =  awful.util.table.join(
awful.button({}, 1, function() -- left click
  os.execute(string.format("%s set %s toggle", volume.cmd, volume.togglechannel or volume.channel))
  volume.update()
end),
awful.button({}, 4, function() -- scroll up
  os.execute(string.format("%s set %s 1%%+", volume.cmd, volume.channel))
  volume.update()
end),
awful.button({}, 5, function() -- scroll down
  os.execute(string.format("%s set %s 1%%-", volume.cmd, volume.channel))
  volume.update()
end)
)
volume.widget:buttons(volume_buttons)
volume_icon:buttons(volume_buttons)--]]

local memory_icon = make_bar_icon("memory")
local memory = lain.widget.mem({
  settings = function()
    widget:set_markup(markup(beautiful.widget_memory or beautiful.wibar_fg, mem_now.used .. " MB, " .. mem_now.swapused .. " MB"))
  end
})

local cpu_icon = make_bar_icon("cpu")
local cpu = lain.widget.cpu({
  settings = function()
    widget:set_markup(markup(beautiful.widget_cpu or beautiful.wibar_fg, cpu_now.usage .. "%"))
  end
})

local cpu_temp_icon = make_bar_icon("cpu_temp")
local cpu_temp = wibox.widget.textbox()
  awful.widget.watch(
  'bash -c "cat /sys/class/hwmon/hwmon1/temp1_input"',
  1,
  function(widget, stdout, stderr)
    widget:set_markup(markup(beautiful.widget_cpu or beautiful.wibar_fg, math.floor(tonumber(stdout) / 1000) .. "°C"))
  end,
  cpu_temp
)

local gpu_icon = make_bar_icon("gpu")
local gpu = wibox.widget.textbox()
  awful.widget.watch(
  'bash -c "radeontop -d - -l 1"',
  1,
  function(widget, stdout, stderr)
    local usage = math.floor(tonumber(stdout:match("gpu (.-)%%")))
    local vram = math.floor(tonumber(stdout:match("vram .- (.-)mb")))

    widget:set_markup(markup(beautiful.widget_gpu or beautiful.wibar_fg, usage .. "%, " .. vram .. " MB"))
  end,
  gpu
)

local gpu_temp_icon = make_bar_icon("gpu_temp")
local gpu_temp = wibox.widget.textbox()
awful.widget.watch(
  'bash -c "cat /sys/class/hwmon/hwmon0/temp1_input"',
  1,
  function(widget, stdout, stderr)
    widget:set_markup(markup(beautiful.widget_gpu or beautiful.wibar_fg, math.floor(tonumber(stdout) / 1000) .. "°C"))
  end,
  gpu_temp
)

local packages_icon = make_bar_icon("packages")
local packages = wibox.widget.textbox()
local packages_wrapper = wibox.widget(status_box({
  packages_icon,
  packages,
  spacing = 2,
  layout = wibox.layout.fixed.horizontal,
}))

awful.widget.watch(
  'bash -c "xbps-install -Mun | wc -l"',
  900,
  function(widget, stdout, stderr)
    local count = tonumber(stdout)
    if count == 0 then
      packages_wrapper.visible = false
      widget:set_markup("")
    else
      packages_wrapper.visible = true
      local str = count .. " pkg" .. (count > 1 and "s" or "")
      widget:set_markup(markup(beautiful.widget_packages or beautiful.wibar_fg, str))
    end
  end,
  packages
)

--[[local net_up_icon = make_bar_icon("net_up")
local net_down_icon = make_bar_icon("net_down")
local net_down = wibox.widget.textbox()
local net_up = lain.widget.net({
  units = 1,
  settings = function()
    widget:set_markup(markup(beautiful.widget_net_up or beautiful.wibar_fg, utils.format_size(net_now.sent)))
    net_down:set_markup(markup(beautiful.widget_net_down or beautiful.wibar_fg, utils.format_size(net_now.received)))
  end,
})--]]

local music_icon = make_bar_icon("music")
local music = wibox.widget.textbox()
local music_wrapper = wibox.widget(status_box({
  music_icon,
  music,
  spacing = 2,
  layout = wibox.layout.fixed.horizontal,
}))

awful.widget.watch(
  'bash -c "~/.config/awesome/scripts/cmus-wrapper.sh"',
  1,
  function(widget, stdout, stderr, _, code)
    if code ~= 0 or stderr:match("^cmus-remote:") or stdout == "" then
      music_wrapper.visible = false

      widget:set_markup("")
    else
      music_wrapper.visible = true

      local nowplaying

      local status = stdout:match("status (.-)\n")
      local duration = tonumber(stdout:match("duration (.-)\n"))
      local position = tonumber(stdout:match("position (.-)\n"))

      if status == "stopped" then
        nowplaying = "[stopped]"
      else
        if duration == -1 and stdout:find("stream ") then
          local stream = stdout:match("stream (.-)\n"):gsub("^%s*(.-)%s*$", "%1")

          nowplaying = stream .. " [" .. utils.format_time(position) .. "]"
        else
          local artist = stdout:match("tag artist (.-)\n"):gsub("^%s*(.-)%s*$", "%1")
          local title = stdout:match("tag title (.-)\n"):gsub("^%s*(.-)%s*$", "%1")

          nowplaying = artist .. " - " .. title
          nowplaying = nowplaying .. " [" .. utils.format_time(position) .. "/" .. utils.format_time(duration) .. "]"
        end

        if status == "paused" then
          nowplaying = nowplaying .. " [" .. status .. "]"
        end
      end

      widget:set_markup(markup(beautiful.widget_music, gears.string.xml_escape(nowplaying)))
    end
  end,
  music
)

local start = wibox.widget({
  {
    {
      {
        wibox.widget.imagebox(beautiful.awesome_icon),
        margins = 1,
        layout = wibox.container.margin,
      },
      --wibox.widget.textbox("Start"),
      spacing = 2,
      layout = wibox.layout.fixed.horizontal,
    },
    margins = 2,
    layout = wibox.container.margin,
  },
  shape = win9x.button,
  layout = wibox.container.background,
})
start:buttons(gears.table.join(
  awful.button({}, 1, function()
    menu.mainmenu:toggle()
    --[[if menu.mainmenu.wibox.visible then
      start.shape = win9x.inset_button
    else
      start.shape = win9x.button
    end--]]
  end)
))

awful.screen.connect_for_each_screen(function(s)
  -- Wallpaper
  utils.set_wallpaper(s)

  -- Each screen has its own tag table.
  awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

  -- Create a promptbox for each screen
  s.__promptbox = awful.widget.prompt()

  -- Create an imagebox widget which will contain an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  s.__layoutbox = awful.widget.layoutbox(s)
  s.__layoutbox:buttons(layout_buttons)

  -- Create a taglist widget
  s.__taglist = awful.widget.taglist({
    screen  = s,
    filter  = awful.widget.taglist.filter.all,
    buttons = taglist_buttons
  })

  -- Create a tasklist widget
  s.__tasklist = awful.widget.tasklist({
    screen = s,
    filter = awful.widget.tasklist.filter.currenttags,
    buttons = tasklist_buttons,
    layout = {
      spacing = 3,
      layout = wibox.layout.fixed.horizontal,
    },
    widget_template = {
      {
        {
          {
            {
              {
                id = "icon_role",
                widget = wibox.widget.imagebox,
              },
              margins = 1,
              layout = wibox.container.margin,
            },
            {
              id = "text_role",
              widget = wibox.widget.textbox,
            },
            spacing = 4,
            layout = wibox.layout.fixed.horizontal
          },
          margins = 2,
          layout = wibox.container.margin,
        },
        id = "background_role",
        widget = wibox.container.background,
      },
      width = 160,
      strategy = "max",
      layout = wibox.container.constraint,
    },
  })

  -- Create the wibox
  s.__bar = awful.wibar({
    position = "top",
    screen = s
  })

  -- Add widgets to the wibox
  s.__bar:setup({
    {
      {
        -- Left widgets
        {
          start,
          s.__promptbox,
          make_divider(),
          s.__taglist,
          {
            s.__layoutbox,
            margins = 3,
            layout = wibox.container.margin,
          },
          make_divider(),
          layout = wibox.layout.fixed.horizontal,
        },

        -- Middle widgets
        s.__tasklist,

        -- Right widgets
        {
          music_wrapper,
          status_box({
            cpu_icon,
            cpu.widget,
            cpu_temp_icon,
            cpu_temp,
            spacing = 2,
            layout = wibox.layout.fixed.horizontal,
          }),
          status_box({
            gpu_icon,
            gpu,
            gpu_temp_icon,
            gpu_temp,
            spacing = 2,
            layout = wibox.layout.fixed.horizontal,
          }),
          status_box({
            memory_icon,
            memory.widget,
            layout = wibox.layout.fixed.horizontal,
          }),
          --[[
          {
            {
              net_down_icon,
              net_down,
              layout = wibox.layout.fixed.horizontal,
            },
            top = 2,
            bottom = 2,
            left = 4,
            right = 4,
            layout = wibox.container.margin,
          },
          {
            {
              net_up_icon,
              net_up.widget,
              layout = wibox.layout.fixed.horizontal,
            },
            top = 2,
            bottom = 2,
            left = 4,
            right = 4,
            layout = wibox.container.margin,
          },--]]
          packages_wrapper,
          make_inset({
            wibox.widget.systray(true),
            {
              {
                uclock,
                clock,
                layout = wibox.layout.fixed.horizontal,
              },
              left = 3,
              right = 3,
              layout = wibox.container.margin,
            },
            spacing = 2,
            layout = wibox.layout.fixed.horizontal,
          }),
          spacing = 4,
          layout = wibox.layout.fixed.horizontal,
        },
        layout = wibox.layout.align.horizontal,
      },
      top = 2,
      left = 2,
      right = 2,
      bottom = 4,
      layout = wibox.container.margin,
    },
    shape = win9x.taskbar,
    layout = wibox.container.background,
  })
end)
