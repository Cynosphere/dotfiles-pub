local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")

local vars = require("modules/vars")

local modkey = vars.modkey

local clientkeys = gears.table.join(
  awful.key(
    {modkey, "Shift"}, "f",
    function(c)
      c.fullscreen = not c.fullscreen
      c:raise()
    end,
    {
      description = "toggle fullscreen",
      group = "client",
    }
  ),
  awful.key(
    {modkey}, "q",
    function(c)
      c:kill()
    end,
    {
      description = "close",
      group = "client",
    }
  ),
  awful.key(
    {modkey}, "f",
    awful.client.floating.toggle,
    {
      description = "toggle floating",
      group = "client",
    }
  ),
  awful.key(
    {modkey}, "t",
    function(c)
      c.sticky = not c.sticky
    end,
    {
      description = "toggle sticky",
      group = "client",
    }
  ),
  awful.key(
    {modkey, "Shift"}, "t",
    function(c)
      c.ontop = not c.ontop
    end,
    {
      description = "toggle keep on top",
      group = "client",
    }
  ),
  awful.key(
    {modkey}, "m",
    function(c)
      c.maximized = not c.maximized
      c:raise()
    end,
    {
      description = "(un)maximize",
      group = "client",
    }
  ),
  awful.key(
    {modkey, "Control"}, "m",
    function (c)
      c.maximized_vertical = not c.maximized_vertical
      c:raise()
    end,
    {
      description = "(un)maximize vertically",
      group = "client",
    }
  ),
  awful.key(
    {modkey, "Shift"}, "m",
    function(c)
      c.maximized_horizontal = not c.maximized_horizontal
      c:raise()
    end,
    {
      description = "(un)maximize horizontally",
      group = "client",
    }
  ),
  awful.key(
    {modkey, "Shift"}, "r",
    function(c)
      if not c.floating then
        c.floating = true
      end

      local scr = awful.screen.focused({client = true})
      local x = scr.geometry.x + (scr.geometry.width / 2 - 800)
      local y = scr.geometry.y + (scr.geometry.height / 2 - 450)
      c:geometry({
        x = x,
        y = y,
        width = 1600,
        height = 900,
      })
    end,
    {
      description = "resize to 1600x900",
      group = "client",
    }
  ),
  awful.key(
    {modkey}, "i",
    function(c)
      local text = "class: " .. c.class
      text = text .. "\ninstance: " .. c.instance
      text = text .. "\nrole: " .. (c.role or "<none>")
      text = text .. "\ntype: " .. c.type
      naughty.notify({
        title = c.name,
        text = text,
      })
    end,
    {
      description = "dump client info",
      group = "client",
    }
  )
)

local titlebar_positions = {"top", "bottom", "left", "right"}
local clientbuttons = gears.table.join(
  awful.button({}, 1, function(c)
    c:emit_signal("request::activate", "mouse_click", {raise = true})
  end),
  awful.button({modkey}, 1, function(c)
    c:emit_signal("request::activate", "mouse_click", {raise = true})
    awful.mouse.client.move(c)
  end),
  awful.button({modkey}, 3, function(c)
    c:emit_signal("request::activate", "mouse_click", {raise = true})
    awful.mouse.client.resize(c)
  end),
  awful.button({modkey, "Shift"}, 1, function(c)
    for _, pos in ipairs(titlebar_positions) do
      awful.titlebar.hide(c, pos)
    end
  end),
  awful.button({modkey, "Shift"}, 3, function(c)
    for _, pos in ipairs(titlebar_positions) do
      awful.titlebar.show(c, pos)
    end
  end)
)

return {
  buttons = clientbuttons,
  keys = clientkeys,
}
