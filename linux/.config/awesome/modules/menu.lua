-- luacheck: globals awesome

-- TODO: fix icons for freedesktop menu to match icon theme
local awful = require("awful")
local beautiful = require("beautiful")
local freedesktop = require("freedesktop")
local hotkeys_popup = require("awful.hotkeys_popup")
local menubar = require("menubar")

local vars = require("modules/vars")

local awesomemenu = {
  {
    "hotkeys",
    function()
      hotkeys_popup.show_help(nil, awful.screen.focused())
    end
  },
  {
    "manual",
    vars.programs.terminal.cmd .. " man awesome"
  },
  {
    "edit config",
    vars.programs.editor.cmd .. " " .. awesome.conffile
  },
  {
    "restart",
    awesome.restart
  },
  {
    "quit",
    function()
      awesome.quit()
    end
  },
}

local mainmenu = freedesktop.menu.build({
  before = {
    {
      "awesome",
      awesomemenu,
      beautiful.awesome_icon
    },
  },
  after = {
    {
      "open terminal",
      vars.programs.terminal.program
    },
  },
})

local launcher = awful.widget.launcher({
  image = beautiful.awesome_icon,
  menu = mainmenu,
})

-- Menubar configuration
menubar.utils.terminal = vars.programs.terminal.program -- Set the terminal for applications that require it

return {
  launcher = launcher,
  mainmenu = mainmenu,
  hotkeys_popup = hotkeys_popup,
}
