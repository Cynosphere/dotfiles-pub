local beautiful = require("beautiful")
local gears = require("gears")

local function button(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(0, 0, width - 1, 1)
  cr:rectangle(0, 1, 1, height - 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_outer))
  cr:rectangle(0, height - 1, width, 1)
  cr:rectangle(width - 1, 0, 1, height - 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_dark))
  cr:rectangle(1, height - 2, width - 2, 1)
  cr:rectangle(width - 2, 1, 1, height - 2)
  cr:fill()
end
local function inset(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_outer))
  cr:rectangle(0, 0, width - 1, 1)
  cr:rectangle(0, 1, 1, height - 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(0, height - 1, width, 1)
  cr:rectangle(width - 1, 0, 1, height - 1)
  cr:fill()
end
local function inset_button(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_outer))
  cr:rectangle(0, 0, width - 1, 1)
  cr:rectangle(0, 1, 1, height - 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(0, height - 1, width, 1)
  cr:rectangle(width - 1, 0, 1, height - 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(1, height - 2, width - 2, 1)
  cr:rectangle(width - 2, 1, 1, height - 2)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_dark))
  cr:rectangle(1, 1, width - 2, 1)
  cr:rectangle(1, 2, 1, height - 4)
  cr:fill()
end

local function border_left(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(1, 0, 1, height)
  cr:fill()
end
local function border_right(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_dark))
  cr:rectangle(width - 2, 0, 1, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_outer))
  cr:rectangle(width - 1, 0, 1, height)
  cr:fill()
end
local function border_bottom(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(1, 0, 1, height - 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_dark))
  cr:rectangle(width - 2, 0, 1, height - 2)
  cr:rectangle(1, height - 2, width - 2, 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_outer))
  cr:rectangle(0, height - 1, width, 1)
  cr:rectangle(width - 1, 0, 1, height - 1)
  cr:fill()
end
local function border_top(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(1, 1, width - 3, 1)
  cr:rectangle(1, 2, 1, height - 2)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_dark))
  cr:rectangle(width - 2, 1, 1, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_outer))
  cr:rectangle(width - 1, 0, 1, height)
  cr:fill()
end

local function taskbar(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_dark))
  cr:rectangle(0, height - 2, width, 1)
  cr:fill()
  cr:set_source(gears.color(beautiful.win9x_outer))
  cr:rectangle(0, height - 1, width, 1)
  cr:fill()
end

local function divider(cr, width, height)
  cr:set_source(gears.color(beautiful.win9x_main))
  cr:rectangle(0, 0, width, height)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_bright))
  cr:rectangle(0, 0, width - 1, 1)
  cr:rectangle(0, 1, 1, height - 1)
  cr:fill()

  cr:set_source(gears.color(beautiful.win9x_dark))
  cr:rectangle(width - 1, 0, 1, height - 1)
  cr:rectangle(0, height - 1, width, 1)
  cr:fill()
end

return {
  button = button,
  inset = inset,
  inset_button = inset_button,
  border = {
    left = border_left,
    right = border_right,
    bottom = border_bottom,
    top = border_top,
  },
  taskbar = taskbar,
  divider = divider,
}
