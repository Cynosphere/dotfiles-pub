local beautiful = require("beautiful")
local gears = require("gears")

local function set_wallpaper(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, false)
  end
end

local function format_time(time)
  local out = ""

  if time >= 3600 then
    out = out .. string.format("%02d:", math.floor(time / 3600))
  end

  out = out .. string.format("%02d:%02d", math.floor(time % 3600 / 60), math.floor(time % 60))

  return out
end

local function gm_round(num, idp)
  local mult = 10 ^ (idp or 0)
  return math.floor(num * mult + 0.5) / mult
end
local function format_size(size)
  size = tonumber(size)

  if size <= 0 then return "0" end
  if size < 1024 then return size .. " B" end
  if size < 1024 * 1024 then return gm_round(size / 1024, 2) .. " KB" end
  if size < 1024 * 1024 * 1024 then return gm_round( size / (1024 * 1024), 2) .. " MB" end

  return gm_round(size / (1024 * 1024 * 1024), 2) .. " GB"
end

local function getupvalues(func)
  local info = debug.getinfo(func, "u")
  local upvalues = {}

  for i = 1, info.nups do
    local k, v = debug.getupvalue(func, i)
    upvalues[k] = v
  end

  return upvalues
end

return {
  set_wallpaper = set_wallpaper,
  format_time = format_time,
  round = gm_round,
  format_size = format_size,
  getupvalues = getupvalues,
}
