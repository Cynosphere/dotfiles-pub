-- luacheck: globals awesome client
local awful = require("awful")
local beautiful = require("beautiful")

local clientbinds = require("modules/clientbinds")

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
  -- All clients will match this rule.
  {
    rule = {},
    properties = {
      border_width = 0, --beautiful.border_width,
      border_color = beautiful.border_normal,
      focus = awful.client.focus.filter,
      raise = true,
      keys = clientbinds.keys,
      buttons = clientbinds.buttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen,
    }
  },

  -- Floating clients.
  {
    rule_any = {
      instance = {
        "DTA",  -- Firefox addon DownThemAll.
        "copyq",  -- Includes session name in class.
        "pinentry",
      },
      class = {
        "Arandr",
        "Blueman-manager",
        "Gpick",
        "Kruler",
        "MessageWin",  -- kalarm.
        "Sxiv",
        "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
        "Wpa_gui",
        "veromix",
        "xtightvncviewer",
        "scrcpy",
      },

      -- Note that the name property shown in xprop might be set slightly after creation of the client
      -- and the name shown there might not match defined rules here.
      name = {
        "Event Tester",  -- xev.
      },
      role = {
        "AlarmWindow",  -- Thunderbird's calendar.
        "ConfigManager",  -- Thunderbird's about:config.
        "pop-up",     -- e.g. Google Chrome's (detached) Developer Tools.
        "devtools",
        "toolbox",
      }
    },
    properties = {
      floating = true
    }
  },

  -- Add titlebars to normal clients and dialogs
  {
    rule_any = {
      type = {
        "normal",
        "dialog",
      }
    },
    properties = {
      titlebars_enabled = true
    },
  },

  -- steam windows float + no titlebar
  {
    rule = {
      instance = "Steam",
    },
    properties = {
      floating = true,
      titlebars_enabled = false,
    },
  },

  -- tg media preview fullscreen
  {
    rule = {
      name = "Media viewer",
      instance = "telegram-desktop",
    },
    properties = {
      titlebars_enabled = false,
      floating = true,
      fullscreen = true,
    },
  },

  -- gamescope
  {
    rule = {
      class = "gamescope",
    },
    properties = {
      titlebars_enabled = false,
      floating = true,
      fullscreen = true,
    },
  },
}

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
  -- Set the windows at the slave,
  -- i.e. put it at the end of others instead of setting it master.
  -- if not awesome.startup then awful.client.setslave(c) end

  if
    awesome.startup
    and not c.size_hints.user_position
    and not c.size_hints.program_position
  then
    -- Prevent clients from being unreachable after screen count changes.
    awful.placement.no_offscreen(c)
  end
end)
