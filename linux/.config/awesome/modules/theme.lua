-- luacheck: globals screen
local beautiful = require("beautiful")
local gears = require("gears")

local vars = require("modules/vars")

local config = vars.config
local theme = require("themes/".. config.theme.scheme)

require("icons")(vars.env.ICON_DIR, theme)

if config.font then
  theme.font = config.font
end
if config.gap then
  theme.useless_gap = config.gap
end
if config.border_width then
  theme.border_width              = config.border_width
  theme.menu_border_width         = config.border_width
  theme.notification_border_width = config.border_width
end
if config.icon_theme then
  theme.icon_theme = config.icon_theme
end

theme.menu_height = 16
theme.menu_width = 128

theme.systray_icon_spacing = 2

theme = beautiful.theme_assets.recolor_titlebar(theme, theme.win9x_buttons_unfocus or theme.win9x_unfocus, "normal")
theme = beautiful.theme_assets.recolor_titlebar(theme, theme.win9x_buttons or theme.fg_focus, "focus")
theme = beautiful.theme_assets.recolor_layout(theme, theme.layout_fg)

local lain_layouts = {
  "termfair",
  "centerfair",
  "cascade",
  "cascadetile",
  "centerwork",
  "centerworkh"
}
for _, key in ipairs(lain_layouts) do
  local image = gears.surface.duplicate_surface(theme["layout_" .. key])
  theme["layout_" .. key] = gears.color.recolor_image(image, theme.layout_fg)
end

do
  local image = gears.surface.duplicate_surface(theme.menu_submenu_icon)
  theme.menu_submenu_icon = gears.color.recolor_image(image, theme.menu_fg_normal)
end

local bar_icons = {
  clock = {
    path = "clock",
    color = theme.widget_clock,
  },
  volume = {
    path = "volume",
    color = theme.widget_volume,
  },
  volume_muted = {
    path = "volume_muted",
    color = theme.widget_volume_muted,
  },
  memory = {
    path = "memory",
    color = theme.widget_memory,
  },
  gpu = {
    path = "gpu",
    color = theme.widget_gpu,
  },
  gpu_temp = {
    path = "temp",
    color = theme.widget_gpu,
  },
  cpu = {
    path = "cpu",
    color = theme.widget_cpu,
  },
  cpu_temp = {
    path = "temp",
    color = theme.widget_cpu,
  },
  music = {
    path = "music",
    color = theme.widget_music,
  },
  packages = {
    path = "packages",
    color = theme.widget_packages,
  },
  net_up = {
    path = "net_up",
    color = theme.widget_net_up,
  },
  net_down = {
    path = "net_down",
    color = theme.widget_net_down,
  },
}

for key, icon in pairs(bar_icons) do
  local image = gears.surface.duplicate_surface(vars.env.ICON_DIR .. "/bar/" .. icon.path .. ".png")
  theme["bar_" .. key] = gears.color.recolor_image(image, icon.color or theme.wibar_fg)
end

theme.tasklist_ontop = "^"
theme.tasklist_sticky = "*"
theme.tasklist_above = "⇑"
theme.tasklist_below = "⇓"
theme.tasklist_floating = "~"
theme.tasklist_maximized = "+"
theme.tasklist_maximized_horizontal = "↔"
theme.tasklist_maximized_vertical = "↕"
theme.tasklist_minimized = "_"

if not beautiful.init(theme) then
  beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
end

if config.wallpaper then
  beautiful.wallpaper = config.wallpaper
end
