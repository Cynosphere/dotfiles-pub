-- luacheck: globals awesome root client
local awful = require("awful")
local cyclefocus = require("cyclefocus")
local gears = require("gears")
local menubar = require("menubar")

local menu = require("modules/menu")
local vars = require("modules/vars")

local modkey = vars.modkey
local altkey = vars.altkey

-- Mouse bindings
local buttons = gears.table.join(
  awful.button({}, 3, function()
    menu.mainmenu:toggle()
  end),
  awful.button({}, 4, awful.tag.viewnext),
  awful.button({}, 5, awful.tag.viewprev)
)

-- Key bindings
local keys = gears.table.join(
  -- awesome
  awful.key(
    {modkey, altkey}, "r",
    awesome.restart,
    {
      description = "reload awesome",
      group = "awesome",
    }
  ),
  awful.key(
    {modkey, altkey}, "q",
    awesome.quit,
    {
      description = "quit awesome",
      group = "awesome",
    }
  ),
  awful.key(
    {modkey}, "s",
    menu.hotkeys_popup.show_help,
    {
      description="show help",
      group = "awesome",
    }
  ),
  awful.key(
    {modkey}, "w",
    function()
      menu.mainmenu:show()
    end,
    {
      description = "show main menu",
      group = "awesome",
    }
  ),
  awful.key(
    {modkey}, "x",
    function()
      awful.prompt.run({
        prompt       = "Run Lua code: ",
        textbox      = awful.screen.focused().__promptbox.widget,
        exe_callback = awful.util.eval,
        history_path = awful.util.get_cache_dir() .. "/history_eval"
      })
    end,
    {
      description = "lua execute prompt",
      group = "awesome",
    }
  ),

  -- launcher
  awful.key(
    {modkey}, "Return",
    function()
      awful.spawn(vars.programs.terminal.program)
    end,
    {
      description = "open a terminal",
      group = "launcher",
    }
  ),
  awful.key(
    {"Control", altkey}, "p",
    function()
      menubar.show()
    end,
    {
      description = "open launcher",
      group = "launcher",
    }
  ),
  awful.key(
    {modkey}, "r",
    function()
      awful.screen.focused().__promptbox:run()
    end,
    {
      description = "run prompt",
      group = "launcher",
    }
  ),
  awful.key(
    {}, "Print",
    function()
      awful.spawn.with_line_callback("screenie-min", {
        stdout = function(path)
          awful.menu({
            {
              "Upload Image",
              function()
                awful.spawn('elixiremanager.sh "' .. path .. '"')
              end,
            },
            {
              "Copy Image",
              function()
                awful.spawn('xclip -selection clipboard -target image/png "' .. path .. '"')
              end,
            }
          }):show()
        end,
      })
    end,
    {
      description = "run screenshot tool",
      group = "launcher",
    }
  ),

  -- client
  awful.key(
    {modkey}, "Up",
    function()
      awful.client.focus.byidx(1)
    end,
    {
      description = "focus next by index",
      group = "client",
    }
  ),
  awful.key(
    {modkey}, "Down",
    function()
      awful.client.focus.byidx(-1)
    end,
    {
      description = "focus previous by index",
      group = "client",
    }
  ),
  awful.key(
    {modkey, "Shift"}, "Up",
    function()
      awful.client.swap.byidx(1)
    end,
    {
      description = "swap with next client by index",
      group = "client",
    }
  ),
  awful.key(
    {modkey, "Shift"}, "Down",
    function()
      awful.client.swap.byidx(-1)
    end,
    {
      description = "swap with previous client by index",
      group = "client",
    }
  ),
  awful.key(
    {modkey}, "u",
    awful.client.urgent.jumpto,
    {
      description = "jump to urgent client",
      group = "client",
    }
  ),
  --[[awful.key(
    {altkey}, "Tab",
    function()
      awful.client.focus.history.previous()
      if client.focus then
        client.focus:raise()
      end
    end,
    {
      description = "go back",
      group = "client",
    }
  ),--]]
  cyclefocus.key(
    {altkey}, "Tab"
  ),
  cyclefocus.key(
    {altkey, "Shift"}, "Tab"
  ),

  -- layout
  awful.key(
    {modkey}, "Right",
    function()
      awful.tag.incmwfact(0.05)
    end,
    {
      description = "increase master width factor",
      group = "layout",
    }
  ),
  awful.key(
    {modkey}, "Left",
    function()
      awful.tag.incmwfact(-0.05)
    end,
    {
      description = "decrease master width factor",
      group = "layout",
    }
  ),
  awful.key(
    {modkey, "Control"}, "Up",
    function()
      awful.tag.incnmaster(1, nil, true)
    end,
    {
      description = "increase the number of master clients",
      group = "layout",
    }
  ),
  awful.key(
    {modkey, "Control"}, "Down",
    function()
      awful.tag.incnmaster(-1, nil, true)
    end,
    {
      description = "decrease the number of master clients",
      group = "layout",
    }
  ),
  awful.key(
    {modkey, "Control"}, "Right",
    function()
      awful.tag.incncol(1, nil, true)
    end,
    {
      description = "increase the number of columns",
      group = "layout",
    }
  ),
  awful.key(
    {modkey, "Control"}, "Left",
    function()
      awful.tag.incncol(-1, nil, true)
    end,
    {
      description = "decrease the number of columns",
      group = "layout",
    }
  ),
  awful.key(
    {modkey}, "Tab",
    function()
      awful.layout.inc(1)
    end,
    {
      description = "select next layout",
      group = "layout",
    }
  ),
  awful.key(
    {modkey, "Shift"}, "Tab",
    function()
      awful.layout.inc(-1)
    end,
    {
      description = "select previous layout",
      group = "layout",
    }
  ),

  -- cmus
  -- TODO: configurability
  awful.key(
    {}, "XF86AudioPlay",
    function()
      awful.spawn("cmus-remote -u")
    end,
    {
      description = "play/pause",
      group = "cmus",
    }
  ),
  awful.key(
    {}, "XF86AudioPrev",
    function()
      awful.spawn("cmus-remote -r")
    end,
    {
      description = "previous track",
      group = "cmus",
    }
  ),
  awful.key(
    {}, "XF86AudioNext",
    function()
      awful.spawn("cmus-remote -n")
    end,
    {
      description = "next track",
      group = "cmus",
    }
  )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
  keys = gears.table.join(keys,
    -- View tag only.
    awful.key(
      {modkey}, "#" .. i + 9,
      function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          tag:view_only()
        end
      end,
      {
        description = "view tag #"..i,
        group = "tag",
      }
    ),
    -- Toggle tag display.
    awful.key(
      {modkey, "Control"}, "#" .. i + 9,
      function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end,
      {
        description = "toggle tag #" .. i,
        group = "tag"
      }
    ),
    -- Move client to tag.
    awful.key(
      {modkey, "Shift"}, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:move_to_tag(tag)
          end
        end
      end,
      {
        description = "move focused client to tag #"..i,
        group = "tag",
      }
    ),
    -- Toggle tag on focused client.
    awful.key(
      {modkey, "Control", "Shift"}, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:toggle_tag(tag)
          end
        end
      end,
      {
        description = "toggle focused client on tag #" .. i,
        group = "tag",
      }
    )
  )
end

root.buttons(buttons)
root.keys(keys)
