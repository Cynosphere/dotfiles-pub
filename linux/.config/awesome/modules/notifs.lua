local beautiful = require("beautiful")
local gears = require("gears")
local lain = require("lain")
local naughty = require("naughty")
local ruled = require("ruled")
local wibox = require("wibox")

local win9x = require("modules/win9x")

ruled.notification.connect_signal("request::rules", function()
  ruled.notification.append_rule({
    rule = {
      urgency = "critical",
    },
    properties = {
      bg = beautiful.fg_urgent,
      fg = beautiful.fg_normal,
      timeout = 0,
      icon_size = 128,
    },
  })

  ruled.notification.append_rule({
    rule = {
      urgency = "normal",
    },
    properties = {
      bg = beautiful.win9x_focus,
      fg = beautiful.fg_normal,
      timeout = 5,
      icon_size = 128,
    },
  })

  ruled.notification.append_rule({
    rule = {
      urgency = "low",
    },
    properties = {
      bg = beautiful.win9x_unfocus,
      fg = beautiful.fg_normal,
      timeout = 5,
      icon_size = 128,
    },
  })
end)

beautiful.notification_icon_size = 128
beautiful.notification_icon_resize_strategy = "resize"

naughty.connect_signal("request::display", function(n)
  -- idk why bg vars dont work but w/e
  local bg = n.bg or beautiful.win9x_focus
  if n.urgency == "critical" then
    bg = beautiful.fg_urgent
  elseif n.urgency == "low" then
    bg = beautiful.win9x_unfocus
  end

  local shadow_title = naughty.widget.title({
    notification = n,
  })
  shadow_title:set_markup_silently(
    lain.util.markup(
      beautiful.__colors.color0,
      gears.string.xml_escape(n.title)
    )
  )

  n.text = n.text:gsub("\\", "\\\\")

  naughty.layout.box({
    notification = n,
    type = "notification",
    position = "top_right",
    border_width = 0,
    icon_size = 128,
    widget_template = {
      {
        {
          {
            wibox.widget({}),
            shape = win9x.border.top,
            forced_height = 4,
            layout = wibox.container.background,
          },
          {
            {
              wibox.widget({}),
              shape = win9x.border.left,
              forced_width = 4,
              layout = wibox.container.background,
            },
            {
              {
                {
                  {
                    {
                      {
                        {
                          shadow_title,
                          left = 1,
                          top = 2,
                          layout = wibox.container.margin,
                        },
                        naughty.widget.title,
                        layout = wibox.layout.stack,
                      },
                      left = 1,
                      right = 4,
                      layout = wibox.container.margin,
                    },
                    {
                      layout = wibox.layout.flex.horizontal
                    },
                    {
                      {
                        {
                          {
                            wibox.widget({}),
                            shape = win9x.button,
                            layout = wibox.container.background,
                          },
                          top = 1,
                          bottom = 1,
                          layout = wibox.container.margin,
                        },
                        {
                          image = beautiful.titlebar_close_button_focus,
                          forced_width = 16,
                          forced_height = 16,
                          widget = wibox.widget.imagebox,
                        },
                        layout = wibox.layout.stack,
                      },
                      right = 1,
                      layout = wibox.container.margin,
                    },
                    layout = wibox.layout.align.horizontal,
                  },
                  margins = 1,
                  layout = wibox.container.margin,
                },
                bg = bg,
                forced_height = 18,
                layout = wibox.container.background,
              },
              {
                wibox.widget({
                  widget = wibox.widget.separator,
                  color = beautiful.win9x_main,
                  orientation = "horizontal",
                  thickness = 2,
                  forced_height = 2,
                  border_width = 0,
                }),
                strategy = "max",
                width = 12,
                layout = wibox.container.constraint,
              },
              layout = wibox.layout.align.vertical,
            },
            {
              wibox.widget({}),
              shape = win9x.border.right,
              forced_width = 4,
              layout = wibox.container.background,
            },
            layout = wibox.layout.align.horizontal,
          },
          {
            {
              wibox.widget({}),
              shape = win9x.border.left,
              forced_width = 4,
              layout = wibox.container.background,
            },
            {
              {
                naughty.widget.icon,
                {
                  naughty.widget.message,
                  naughty.list.actions,
                  spacing = 8,
                  layout = wibox.layout.align.vertical,
                },
                fill_space = true,
                spacing = 8,
                layout = wibox.layout.fixed.horizontal,
              },
              margins = 2,
              layout = wibox.container.margin,
            },
            {
              wibox.widget({}),
              shape = win9x.border.right,
              forced_width = 4,
              layout = wibox.container.background,
            },
            layout = wibox.layout.align.horizontal,
          },
          layout = wibox.layout.align.vertical,
        },
        {
          wibox.widget({}),
          shape = win9x.border.bottom,
          forced_height = 4,
          layout = wibox.container.background,
        },
        layout = wibox.layout.align.vertical,
      },
      bg = beautiful.win9x_main,
      layout = wibox.container.background,
    },
  })
end)

--[[for _, urgency in ipairs({"normal", "critical", "low"}) do
  naughty.notification({
    title = urgency .. " example",
    text = "some text",
    icon = beautiful.awesome_icon,
    urgency = urgency,
    timeout = 10,
  })
end--]]
