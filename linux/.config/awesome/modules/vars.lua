local gears = require("gears")

local HOME = os.getenv("HOME")
local CONFIG_DIR = gears.filesystem.get_xdg_config_home() .. "awesome/"
local ICON_DIR = CONFIG_DIR .. "icons"
local THEME_DIR = CONFIG_DIR .. "themes"

local config = require("config")
local terminal = config.terminal or "xterm"
local terminal_separator = config.terminal_separator or "-e"
local editor = config.editor or os.getenv("EDITOR") or "nano"
local modkey = config.modkey or "Mod4"
local altkey = config.altkey or "Mod1"

local vars = {
  config = config,
  env = {
    HOME = HOME,
    CONFIG_DIR = CONFIG_DIR,
    ICON_DIR = ICON_DIR,
    THEME_DIR = THEME_DIR,
  },
  modkey = modkey,
  altkey = altkey,
  programs = {
    terminal = {
      program = terminal,
      separator = terminal_separator,
      cmd = terminal .. " " .. terminal_separator,
    },
    editor = {
      program = editor,
      cmd = terminal .. " " .. terminal_separator .. " " .. editor,
    },
  },
}

return vars
