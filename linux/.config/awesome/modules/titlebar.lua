-- luacheck: globals client
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local lain = require("lain")
local wibox = require("wibox")

local vars = require("modules/vars")
local win9x = require("modules/win9x")

local function make_win9x_button(child)
  return {
    {
      {
        wibox.widget({}),
        shape = win9x.button,
        layout = wibox.container.background,
      },
      top = 1,
      bottom = 1,
      layout = wibox.container.margin,
    },
    child,
    layout = wibox.layout.stack,
  }
end

local function _serenity_lines(cr, width, height, focus)
  local col = focus and beautiful.__colors.color0 or beautiful.win9x_outer

  local y = 0
  for i = 1, math.ceil(height / 2) do
    cr:set_source(gears.color(col))
    cr:rectangle(0, y, width, 1)
    cr:fill()

    y = y + 2
  end
end
local function _serenity_lines_focus(cr, width, height)
  _serenity_lines(cr, width, height, true)
end
local function _serenity_lines_normal(cr, width, height)
  _serenity_lines(cr, width, height, false)
end

-- Add a titlebar if titlebars_enabled is set to true in the rules.
local serenity = vars.config.theme.serenity_titlebar or false
client.connect_signal("request::titlebars", function(c)
  -- buttons for the titlebar
  local buttons = gears.table.join(
    awful.button({}, 1, function()
      c:emit_signal("request::activate", "titlebar", {raise = true})
      awful.mouse.client.move(c)
    end),
    awful.button({}, 3, function()
      c:emit_signal("request::activate", "titlebar", {raise = true})
      awful.mouse.client.resize(c)
    end)
  )

  awful.titlebar(c, {
    position = "left",
    size = 4,
    bg_normal = "#00000000",
    bg_focus = "#00000000",
  }):setup({
    wibox.widget({}),
    shape = win9x.border.left,
    layout = wibox.container.background,
  })
  awful.titlebar(c, {
    position = "right",
    size = 4,
    bg_normal = "#00000000",
    bg_focus = "#00000000",
  }):setup({
    wibox.widget({}),
    shape = win9x.border.right,
    layout = wibox.container.background,
  })

  awful.titlebar(c, {
    position = "bottom",
    size = 4,
    bg_normal = "#00000000",
    bg_focus = "#00000000",
  }):setup({
    wibox.widget({}),
    shape = win9x.border.bottom,
    layout = wibox.container.background,
  })

  local lines = wibox.widget({
    wibox.widget({}),
    shape = c.focus and _serenity_lines_focus or _serenity_lines_normal,
    layout = wibox.container.background,
  })
  local function update_lines(focus)
    lines:set_shape(focus and _serenity_lines_focus or _serenity_lines_normal)
  end

  local shadow_title = awful.titlebar.widget.titlewidget(c)
  shadow_title.update = function() end
  local name = gears.string.xml_escape(c.name or awful.titlebar.fallback_name)
  shadow_title:set_markup_silently(lain.util.markup(beautiful.win9x_title_unfocus or beautiful.win9x_outer, name))

  c:connect_signal("focus", function()
    name = gears.string.xml_escape(c.name or awful.titlebar.fallback_name)
    shadow_title:set_markup_silently(lain.util.markup(beautiful.win9x_title_focus or beautiful.__colors.color, name))
    update_lines(true)
  end)
  c:connect_signal("unfocus", function()
    name = gears.string.xml_escape(c.name or awful.titlebar.fallback_name)
    shadow_title:set_markup_silently(lain.util.markup(beautiful.win9x_title_unfocus or beautiful.win9x_outer, name))
    update_lines(false)
  end)
  c:connect_signal("property::name", function()
    name = gears.string.xml_escape(c.name or awful.titlebar.fallback_name)
    local focused = c == client.focus
    local col = focused and (beautiful.win9x_title_focus or beautiful.__colors.color0) or (beautiful.win9x_title_unfocus or beautiful.win9x_outer)
    shadow_title:set_markup_silently(lain.util.markup(col, name))
  end)

  local titlebar = awful.titlebar(c, {
    size = serenity and 25 or 24,
    fg_normal = beautiful.win9x_unfocus,
    fg_focus = beautiful.fg_focus,
    bg_normal = beautiful.win9x_dark,
    bg_focus = beautiful.win9x_focus,
  })

  titlebar:setup({
    {
      wibox.widget({}),
      forced_height = 4,
      shape = win9x.border.top,
      layout = wibox.container.background,
    },
    {
      {
        wibox.widget({}),
        forced_width = 4,
        shape = win9x.border.left,
        layout = wibox.container.background,
      },
      {
        {
          { -- Left
            {
              awful.titlebar.widget.iconwidget(c),
              left = 2,
              right = 2,
              top = serenity and 2 or 1,
              bottom = 1,
              layout = wibox.container.margin,
            },
            {
              {
                shadow_title,
                top = serenity and 1 or 2,
                left = 1,
                layout = wibox.container.margin,
              },
              awful.titlebar.widget.titlewidget(c),
              layout = wibox.layout.stack,
            },
            buttons = buttons,
            spacing = 2,
            layout = wibox.layout.fixed.horizontal
          },
          { -- Middle
            serenity and {
              lines,
              top = 2,
              bottom = 2,
              left = 2,
              layout = wibox.container.margin,
            } or nil,
            buttons = buttons,
            layout = wibox.layout.flex.horizontal
          },
          { -- Right
            {
              {
                make_win9x_button(awful.titlebar.widget.ontopbutton(c)),
                make_win9x_button(awful.titlebar.widget.stickybutton(c)),
                make_win9x_button(awful.titlebar.widget.floatingbutton(c)),
                layout = wibox.layout.fixed.horizontal,
              },
              {
                make_win9x_button(awful.titlebar.widget.minimizebutton(c)),
                make_win9x_button(awful.titlebar.widget.maximizedbutton(c)),
                layout = wibox.layout.fixed.horizontal,
              },
              make_win9x_button(awful.titlebar.widget.closebutton(c)),
              spacing = 2,
              layout = wibox.layout.fixed.horizontal
            },
            left = 2,
            right = 2,
            top = 1,
            bottom = serenity and 2 or 1,
            layout = wibox.container.margin,
          },
          forced_height = serenity and 19 or 18,
          layout = wibox.layout.align.horizontal,
        },
        {
          wibox.widget({
            widget = wibox.widget.separator,
            color = beautiful.win9x_main,
            orientation = "horizontal",
            thickness = 2,
            forced_height = 2,
            border_width = 0,
          }),
          forced_height = 2,
          layout = wibox.layout.fixed.vertical,
        },
        layout = wibox.layout.align.vertical,
      },
      {
        wibox.widget({}),
        forced_width = 4,
        shape = win9x.border.right,
        layout = wibox.container.background,
      },
      layout = wibox.layout.align.horizontal,
    },
    layout = wibox.layout.align.vertical,
  })
end)

-- Enable sloppy focus, so that focus follows mouse.
--[[client.connect_signal("mouse::enter", function(c)
  c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c)
  c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
  c.border_color = beautiful.border_normal
end)
--]]
