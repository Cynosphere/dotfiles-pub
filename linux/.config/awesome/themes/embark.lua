local vars = require("modules/vars")

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")({
    background = "#1E1C31",
    foreground = "#CBE3E7",
    secondary  = "#181627",
    tertiary   = "#100E23",
    accent     = "#3E3859",

    color0 = "#2D2B40",
    color1 = "#F48FB1",
    color2 = "#A1EFD3",
    color3 = "#FFE6B3",
    color4 = "#91DDFF",
    color5 = "#D4BFFF",
    color6 = "#87DFEB",
    color7 = "#8A889D",

    color8  = "#100E23",
    color9  = "#F02E6E",
    color10 = "#62D196",
    color11 = "#F2B482",
    color12 = "#65B2FF",
    color13 = "#A37ACC",
    color14 = "#63F2F1",
    color15 = "#585273",
})

theme.win9x_bright = "#2D2B40"
theme.win9x_buttons_unfocus = "#2D2B40"
theme.win9x_unfocus = "#8A889D"

return theme
