local vars = require("modules/vars")

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")({
    background = "#36393f",
    foreground = "#dcddde",
    secondary  = "#2f3136",
    tertiary   = "#292b2f",
    accent     = "#5865f2",

    color0 = "#202225",
    color1 = "#ed4245",
    color2 = "#46c46d",
    color3 = "#faa61a",
    color4 = "#00b0f4",
    color5 = "#b377f3",
    color6 = "#45ddc0",
    color7 = "#a3a6aa",

    color8  = "#4f545c",
    color9  = "#f1686a",
    color10 = "#57f287",
    color11 = "#fbb848",
    color12 = "#33c0f6",
    color13 = "#d09aff",
    color14 = "#86dcc5",
    color15 = "#b9bbbe",
})

return theme
