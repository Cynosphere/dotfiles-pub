local vars = require("modules/vars")

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")({
    background = "#4c5844",
    foreground = "#f7f8f7",
    secondary  = "#3e4637",
    tertiary   = "#282e22",
    accent     = "#958831",

    color0 = "#282e22",
    color1 = "#aa0000",
    color2 = "#4d7455",
    color3 = "#cf6a32",
    color4 = "#476291",
    color5 = "#8650ac",
    color6 = "#5e98d9",
    color7 = "#837e77",

    color8  = "#4c5844",
    color9  = "#eb4b4b",
    color10 = "#70b04a",
    color11 = "#ffd700",
    color12 = "#4b69ff",
    color13 = "#8847ff",
    color14 = "#38f3ab",
    color15 = "#b2b2b2",
})

theme.win9x_bright = "#889180"
theme.win9x_unfocus = "#889180"

return theme
