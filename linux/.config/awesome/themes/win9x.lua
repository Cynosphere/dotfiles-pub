local vars = require("modules/vars")

local colors = {
    background = "#cccccc",
    foreground = "#ffffff",
    secondary  = "#444444",
    tertiary   = "#000000",
    accent     = "#000080",

    color0 = "#000000",
    color1 = "#880000",
    color2 = "#008800",
    color3 = "#888800",
    color4 = "#000088",
    color5 = "#880088",
    color6 = "#008888",
    color7 = "#888888",

    color8  = "#cccccc",
    color9  = "#ff0000",
    color10 = "#00ff00",
    color11 = "#ffff00",
    color12 = "#0000ff",
    color13 = "#ff00ff",
    color14 = "#00ffff",
    color15 = "#ffffff",
}

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")(colors)

theme.win9x_main = colors.background
theme.win9x_bright = "#ffffff"
theme.win9x_dark = colors.secondary
theme.win9x_outer = colors.tertiary
theme.win9x_focus = colors.accent
theme.win9x_unfocus = colors.color8
theme.win9x_buttons = "#000000"
theme.win9x_buttons_unfocus = colors.secondary

return theme
