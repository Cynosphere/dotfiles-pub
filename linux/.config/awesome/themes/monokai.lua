local vars = require("modules/vars")

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")({
    background = "#272822",
    foreground = "#f1ebeb",
    secondary  = "#1d1e19",
    tertiary   = "#161613",
    accent     = "#8fc029",

    color0 = "#48483e",
    color1 = "#dc2566",
    color2 = "#8fc029",
    color3 = "#d4c96e",
    color4 = "#55bcce",
    color5 = "#9358fe",
    color6 = "#56b7a5",
    color7 = "#acada1",

    color8  = "#76715e",
    color9  = "#fa2772",
    color10 = "#a7e22e",
    color11 = "#e7db75",
    color12 = "#66d9ee",
    color13 = "#ae82ff",
    color14 = "#66efd5",
    color15 = "#cfd0c2",
})

return theme
