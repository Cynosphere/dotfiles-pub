local vars = require("modules/vars")

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")({
    background = "#1a1a1a",
    foreground = "#dedbeb",
    secondary  = "#171717",
    tertiary   = "#141414",
    accent     = "#634e75",

    color0 = "#2d2d2d",
    color1 = "#e83f80",
    color2 = "#a2baa8",
    color3 = "#eacac0",
    color4 = "#9985d1",
    color5 = "#e68ac1",
    color6 = "#aabae7",
    color7 = "#dedbeb",

    color8  = "#383838",
    color9  = "#f55d8f",
    color10 = "#bfd1c3",
    color11 = "#f0ddd8",
    color12 = "#b4a4de",
    color13 = "#edabd2",
    color14 = "#c4d1f5",
    color15 = "#edebf7",
})

return theme
