local vars = require("modules/vars")

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")({
    background = "#323c41",
    foreground = "#f2efdf",
    secondary  = "#2b3339",
    tertiary   = "#1e2327",
    accent     = "#93b259",

    color0 = "#7a8478",
    color1 = "#e67e80",
    color2 = "#a7c080",
    color3 = "#dbbc7f",
    color4 = "#7fbbb3",
    color5 = "#d699b6",
    color6 = "#83c092",
    color7 = "#d3c6aa",

    color8 = "#5c6a72",
    color9 = "#f85552",
    color10 = "#8da101",
    color11 = "#dfa000",
    color12 = "#3a94c5",
    color13 = "#df69ba",
    color14 = "#35a77c",
    color15 = "#9da9a0",
})

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
