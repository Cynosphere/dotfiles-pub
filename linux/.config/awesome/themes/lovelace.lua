local vars = require("modules/vars")

local theme = dofile(vars.env.THEME_DIR .. "/_base.lua")({
    background = "#1d1f28",
    foreground = "#ffffff",
    secondary  = "#181a21",
    tertiary   = "#141721",
    accent     = "#c574dd",

    color0 = "#282a36",
    color1 = "#f37f97",
    color2 = "#5adecd",
    color3 = "#f2a272",
    color4 = "#8897f4",
    color5 = "#c574dd",
    color6 = "#79e6f3",
    color7 = "#fdfdfd",

    color8 = "#414458",
    color9 = "#ff4971",
    color10 = "#18e3c8",
    color11 = "#ff8037",
    color12 = "#556fff",
    color13 = "#b043d1",
    color14 = "#3fdcee",
    color15 = "#bebec1",
})

return theme
