-- luacheck: globals awesome screen debug_print
pcall(require, "luarocks.loader")

local awful = require("awful")
local beautiful = require("beautiful")
local cyclefocus = require("cyclefocus")
local lain = require("lain")
local naughty = require("naughty")

require("awful.autofocus")
require("awful.hotkeys_popup.keys")

naughty.connect_signal("request::display_error", function(message, startup)
  naughty.notification({
    urgency = "critical",
    title   = "Oops, an error happened"..(startup and " during startup!" or "!"),
    message = debug.traceback(message),
  })
end)

function debug_print(...)
  local str = ""
  local args = {...}
  for i, val in ipairs(args) do
    str = str .. tostring(val) .. (i == #args and "" or "\n")
  end

  naughty.notification({
    title = "debug",
    text = str
  })
end

local vars = require("modules/vars")

-- Table of layouts to cover with awful.layout.inc, order matters.
-- this HAS to be set before everything so it sets default layout properly
awful.layout.layouts = {}
for _, layout in ipairs(vars.config.layouts) do
  local layoutRef = awful.layout.suit
  if layout:find("^lain:") then
    layoutRef = lain.layout
    layout = layout:gsub("^lain:","")
  end

  if layout:find("%.") then
    local perStart, perEnd = layout:find("%.")
    local first = layout:sub(1, perStart - 1)
    local second = layout:sub(perEnd + 1, #layout)

    table.insert(awful.layout.layouts, layoutRef[first][second])
  else
    table.insert(awful.layout.layouts, layoutRef[layout])
  end
end

local utils = require("modules/utils")
require("modules/theme")
require("modules/bar")
require("modules/binds")
require("modules/windows")
require("modules/titlebar")
require("modules/notifs")

-- cyclefocus settings
cyclefocus.move_mouse_pointer = false
cyclefocus.cycle_filters = {
  function() return true end,
}
cyclefocus.preset_for_offset = {
  default = function(preset, args)
    preset.font = vars.config.font
    preset.icon_size = 16
    local title = cyclefocus.get_client_title(args.client, false)
    local col = args.client.minimized and beautiful.fg_minimize or beautiful.fg_normal
    preset.text = lain.util.markup(col, title)
  end,
  ["0"] = function(preset, args)
    preset.font = vars.config.font
    preset.icon_size = 16
    preset.text = lain.util.markup(beautiful.tasklist_fg_focus, cyclefocus.get_client_title(args.client, false))
  end,
}
cyclefocus.icon_col_width = 2

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", utils.set_wallpaper)

-- Autostart
-- taken from: https://gitlab.com/luna/til/-/blob/main/awesome/rc.lua
awful.spawn.with_line_callback(vars.env.HOME .. "/.local/bin/autostart.sh", {
  stdout = function()
    --[[naughty.notify({
      title = "autostart",
      text = "pid: "..line.." already started",
      timeout = 7,
      position = "top_right"
    })--]]
  end,
  exit = function(reason, code)
    local data = {
      title = "autostart exited",
      text = reason .. " " .. code,
      timeout = 4,
    }

    if reason ~= "exit" and code ~= 0 then
      data.urgency = "critical"
    end

    naughty.notification(data)
  end
})
