#!/bin/bash

state=0
sleep_pid=0

function toggle() {
  state=$(((state+1)%3))

  if [ "$sleep_pid" -ne 0 ]; then
    kill $sleep_pid >/dev/null 2>&1
  fi
}

trap "toggle" USR1

function bclock() {
  local row0=("⠀" "⠈" "⠐" "⠘" "⠠" "⠨" "⠰" "⠸" "⢀" "⢈")
  local row1=("⠁" "⠉" "⠑" "⠙" "⠡" "⠩" "⠱" "⠹" "⢁" "⢉")
  local row2=("⠂" "⠊" "⠒" "⠚" "⠢" "⠪" "⠲" "⠺" "⢂" "⢊")
  local row3=("⠃" "⠋" "⠓" "⠛" "⠣" "⠫" "⠳" "⠻" "⢃" "⢋")
  local row4=("⠄" "⠌" "⠔" "⠜" "⠤" "⠬" "⠴" "⠼" "⢄" "⢌")
  local row5=("⠅" "⠍" "⠕" "⠝" "⠥" "⠭" "⠵" "⠽" "⢅" "⢍")
  local chars=("${row0[@]}" "${row1[@]}" "${row2[@]}" "${row3[@]}" "${row4[@]}" "${row5[@]}")

  local hour=$(date +"%H")
  local minute=$(date +"%M")
  local second=$(date +"%S")
  [[ $hour = 0* ]] && hour=${hour:1:1}
  [[ $minute = 0* ]] && minute=${minute:1:1}
  [[ $second = 0* ]] && second=${second:1:1}

  echo " ${chars[$hour]} ${chars[$minute]} ${chars[$second]}"
}

while true; do
  if [ $state -eq 0 ]; then
    bclock
  elif [ $state -eq 1 ]; then
    echo " $(date +'%H:%M:%S')"
  else
    echo " $(date +'%a, %m/%d/%Y')"
  fi
  sleep 1 &
  sleep_pid=$!
  wait
done
