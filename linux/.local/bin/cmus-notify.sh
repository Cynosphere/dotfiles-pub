#!/bin/bash
[ $# -eq 0 ] && exit 0

get_albumart() {
  rm /tmp/albumart.jpg
  local dir_name=$( dirname "$1" )
  local names=(folder cover albumart front "$2")
  for item in "${names[@]}" ; do
    img_file=$( find "$dir_name" -maxdepth 1 -iregex ".*$item*\.\(jpg\|jpeg\|gif\|png\|\)$" -print -quit )
    [ -n "$img_file" ] && cp "$img_file" /tmp/_albumart.jpg && break
  done
  [ -z "$img_file" ] && ffmpeg -i "$1" -an -vcodec copy "/tmp/_albumart.jpg" -y && img_file="/tmp/_albumart.jpg"
  [ -n "$img_file" ] && ffmpeg -i /tmp/_albumart.jpg -vf scale=72:-1 /tmp/albumart.jpg && rm /tmp/_albumart.jpg
}

if [ $2 == "playing" ]; then
  while [ $# -ge 2 ] ; do
    eval _$1='$2'
    shift
    shift
  done

  get_albumart "$_file" "$_album"

  duration_formatted=""

  if [ "$_duration" != "" ]; then
    if [ $_duration -ge 3600 ]; then
      duration_formatted+=$(printf '%02d:' $(($_duration/3600)))
    fi

    duration_formatted+=$(printf '%02d:%02d' $(($_duration%3600/60)) $(($_duration%60)))
  fi

  details=""

  if [ "$_artist" != "" ]; then
    details+="$_artist"
  fi
  if [ "$_album" != "" ]; then
    if [ "$details" != "" ]; then
      details+="\n"
    fi
    details+="$_album"
  fi
  if [ "$duration_formatted" != "" ]; then
    if [ "$details" != "" ]; then
      details+="\n"
    fi
    details+="$duration_formatted"
  fi

  notify-send -t 5000 -i /tmp/albumart.jpg "$_title" "$details"
fi
