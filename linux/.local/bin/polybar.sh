#!/usr/bin/env sh

pkill polybar

while pgrep -x polybar > /dev/null; do sleep 1; done

polybar bar >> /dev/null 2>&1 &
