set constantshow
#set mouse
set minibar
set linenumbers
#set nohelp
set tabstospaces
set guidestripe 80
set autoindent
set indicator
set tabsize 2

#colors
set minicolor lightmagenta,lightblack
set scrollercolor white,black
set numbercolor lightblack,normal
set errorcolor red,black
set keycolor lightyellow,normal
set functioncolor normal,normal
set stripecolor yellow,black
set promptcolor green,black

#binds
bind ^P linenumbers all
bind ^F tab all
bind ^G unindent all
bind Sh-M-F formatter all
bind Sh-M-D linter all
bind M-Z undo all
bind Sh-M-Z redo all
bind M-X cut all
bind M-C copy all
bind M-V paste all
bind M-. nextbuf all
bind M-, prevbuf all
bind M-D delete all
bind ^W savefile all
bind ^/ whereis all
bind M-/ wherewas all

#syntax highlighting
include ~/.config/nano/syntax/awk.nanorc
include ~/.config/nano/syntax/c.nanorc
include ~/.config/nano/syntax/cmake.nanorc
include ~/.config/nano/syntax/colortest.nanorc
include ~/.config/nano/syntax/conf.nanorc
include ~/.config/nano/syntax/csharp.nanorc
include ~/.config/nano/syntax/css.nanorc
include ~/.config/nano/syntax/csv.nanorc
include ~/.config/nano/syntax/default.nanorc
include ~/.config/nano/syntax/diff.nanorc
include ~/.config/nano/syntax/dotenv.nanorc
include ~/.config/nano/syntax/etc-hosts.nanorc
include ~/.config/nano/syntax/git.nanorc
include ~/.config/nano/syntax/glsl.nanorc
include ~/.config/nano/syntax/go.nanorc
include ~/.config/nano/syntax/gophermap.nanorc
include ~/.config/nano/syntax/gradle.nanorc
include ~/.config/nano/syntax/html.nanorc
include ~/.config/nano/syntax/ini.nanorc
include ~/.config/nano/syntax/inputrc.nanorc
include ~/.config/nano/syntax/java.nanorc
include ~/.config/nano/syntax/javascript.nanorc
include ~/.config/nano/syntax/json.nanorc
include ~/.config/nano/syntax/kotlin.nanorc
include ~/.config/nano/syntax/lua.nanorc
include ~/.config/nano/syntax/m3u.nanorc
include ~/.config/nano/syntax/makefile.nanorc
include ~/.config/nano/syntax/man.nanorc
include ~/.config/nano/syntax/markdown.nanorc
include ~/.config/nano/syntax/nanorc.nanorc
include ~/.config/nano/syntax/nginx.nanorc
include ~/.config/nano/syntax/nmap.nanorc
include ~/.config/nano/syntax/patch.nanorc
include ~/.config/nano/syntax/php.nanorc
include ~/.config/nano/syntax/pkg-config.nanorc
include ~/.config/nano/syntax/pkgbuild.nanorc
include ~/.config/nano/syntax/powershell.nanorc
include ~/.config/nano/syntax/properties.nanorc
include ~/.config/nano/syntax/python.nanorc
include ~/.config/nano/syntax/rust.nanorc
include ~/.config/nano/syntax/sed.nanorc
include ~/.config/nano/syntax/sh.nanorc
include ~/.config/nano/syntax/sql.nanorc
include ~/.config/nano/syntax/systemd.nanorc
include ~/.config/nano/syntax/toml.nanorc
include ~/.config/nano/syntax/ts.nanorc
include ~/.config/nano/syntax/vi.nanorc
include ~/.config/nano/syntax/xml.nanorc
include ~/.config/nano/syntax/xresources.nanorc
include ~/.config/nano/syntax/yaml.nanorc
include ~/.config/nano/syntax/zig.nanorc
include ~/.config/nano/syntax/zsh.nanorc

extendsyntax JavaScript formatter prettier-nano
extendsyntax JavaScript linter eslint -f unix
extendsyntax json formatter prettier-nano
extendsyntax css formatter prettier-nano

#extendsyntax nanorc color ,red "[[:space:]]+$"
#extendsyntax SH color ,red "[[:space:]]+$"
#extendsyntax JavaScript color ,red "[[:space:]]+$"
#extendsyntax JSON color ,red "[[:space:]]+$"
#extendsyntax CSS color ,red "[[:space:]]+$"
#extendsyntax HTML color ,red "[[:space:]]+$"
#extendsyntax Lua color ,red "[[:space:]]+$"
