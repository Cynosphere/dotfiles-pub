local wezterm = require("wezterm");
local config = {
  font_size = 9.0,
  freetype_load_target = "Mono",
  use_cap_height_to_scale_fallback_fonts = true,
  bold_brightens_ansi_colors = true,
  color_scheme = "Amora Focus",
  hide_tab_bar_if_only_one_tab = true,
  window_padding = {
    left = 4,
    right = 4,
    top = 4,
    bottom = 4,
  },
  use_fancy_tab_bar = false,
  tab_bar_at_bottom = false,
  launch_menu = {},
  keys = {
    -- disable search (ctrl f = tab)
    {key = "f", mods = "CTRL|SHIFT", action = "DisableDefaultAssignment"},

    -- launcher
    {key = "Space", mods = "CTRL|SHIFT", action = "ShowLauncher"},

    -- , and . navigation between tabs
    {key = "<", mods = "CTRL|SHIFT", action = wezterm.action.ActivateTabRelative(-1)},
    {key = ">", mods = "CTRL|SHIFT", action = wezterm.action.ActivateTabRelative(1)},
  },
  mouse_bindings = {
    -- Change the default selection behavior so that it only selects text,
    -- but doesn't copy it to a clipboard or open hyperlinks.
    {
      event={Up={streak=1, button="Left"}},
      mods="NONE",
      action=wezterm.action{ExtendSelectionToMouseCursor="Cell"}
    },

    -- Don't automatically copy the selection to the clipboard
    -- when double clicking a word
    {
      event={Up={streak=2, button="Left"}},
      mods="NONE",
      action="Nop",
    },

    -- Ctrl-click will open the link under the mouse cursor
    {
      event={Up={streak=1, button="Left"}},
      mods="CTRL",
      action="OpenLinkAtMouseCursor",
    },
  },
  color_scheme_dirs = {},
}

if wezterm.target_triple == "x86_64-pc-windows-msvc" then
  config.default_prog = {"C:/Program Files/PowerShell/7/pwsh.exe", "-NoLogo"}
  -- config.default_cwd = "D:/Downloads"
  config.font = wezterm.font_with_fallback({
    "Terminus (TTF)",
    "Unifont Windows",
    "SijiNGWindows",
    "Noto Emoji",
  })
  config.unix_domains = {
    {
      name = "unix",
    },
  }
  -- config.default_gui_startup_args = {"connect", "unix"}

  config.color_scheme_dirs[#config.color_scheme_dirs + 1] = "D:/Misc/dotfiles-pub/common/wezterm/colors"

  --[[table.insert(config.launch_menu, {
    label = "MSYS2",
    args = {
      "C:/msys64/msys2_shell.cmd",
      "-no-start","-defterm","-msys2",
    },
  })
  table.insert(config.launch_menu, {
    label = "MinGW64",
    args = {
      "C:/msys64/msys2_shell.cmd",
      "-no-start","-defterm","-mingw64",
    },
  })
  table.insert(config.launch_menu, {
    label = "cygwin",
    args = {
      "D:/Programs/cygwin/bin/bash",
      "--login",
    },
  })--]]
else
  local function fallback(font)
    return wezterm.font_with_fallback({
      font,
      "Fixed",
      "Terminus (TTF)",
      "Sazanami Gothic",
      "Unifont",
      "Noto Emoji",
    })
  end
  config.font = fallback("tewi")
  config.font_rules = {
    {
      intensity = "Bold",
      font = fallback({family = "tewi", style = "Normal"}),
    },
    {
      italic = true,
      font = fallback({family = "orp", style = "Italic"}),
    },
    {
      intensity = "Bold",
      italic = true,
      font = fallback({family = "orp", style = "Italic"}),
    }
  }
  --[[config.font = wezterm.font_with_fallback({
    {family = "tewi", style = "Normal"},
    {family = "tewi", style = "Normal", weight = "Bold"},
    {family = "orp", style = "Italic"},
    {family = "orp", style = "Italic", weight = "Bold"},
    "Terminus (TTF)",
    "Unifont",
    "Noto Emoji",
  })--]]
end

return config
