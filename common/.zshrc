if [[ ! -d "$HOME/.zsh" ]]; then
  mkdir -p "$HOME/.zsh"
fi

fpath=(~/.zsh $fpath)

if [[ -f "$HOME/.zshcfg" ]]; then
  source "$HOME/.zshcfg"
else
  CONF_BINARYCLOCK=0
  CONF_ENABLEGIT=0
  CONF_ENABLEAC=0
  CONF_FORTUNE=0
fi

if [[ $CONF_ENABLEGIT -eq 1 ]]; then
  if [[ ! -d "$HOME/.zsh/gitstatus" ]]; then
    git clone --depth=1 https://github.com/romkatv/gitstatus.git "$HOME/.zsh/gitstatus"
  fi
  source "$HOME/.zsh/gitstatus/gitstatus.plugin.zsh"
fi

if [[ $CONF_ENABLEAC -eq 1 ]]; then
  if [[ ! -d "$HOME/.zsh/zsh-autosuggestions" ]]; then
    git clone --depth=1 https://github.com/zsh-users/zsh-autosuggestions.git "$HOME/.zsh/zsh-autosuggestions"
  fi
  source "$HOME/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh"
fi

if [[ $CONF_ENABLESH -eq 1 ]]; then
  if [[ ! -d "$HOME/.zsh/fast-syntax-highlighting" ]]; then
    git clone --depth=1 https://github.com/zdharma-continuum/fast-syntax-highlighting "$HOME/.zsh/fast-syntax-highlighting"
  fi
fi

download() {
  if [[ -x "$(command -v wget)" ]]; then
    wget -qO "$2" "$1"
    return $?
  elif [[ -x "$(command -v curl)" ]]; then
    curl -fsSL "$1" -o "$2"
    return $?
  fi
  echo "Failed to download $1. curl or wget not installed"
  return 1
}


if [[ ! -f "$HOME/.zsh/z.sh" ]]; then
  download "https://raw.githubusercontent.com/rupa/z/master/z.sh" "$HOME/.zsh/z.sh"
fi
. "$HOME/.zsh/z.sh"

autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt SHARE_HISTORY
# End of lines configured by zsh-newuser-install

####

# prompt
setopt prompt_subst

git_status=""
git_status_v2() {
  git_status=""
  if gitstatus_query PROMPT && [[ $VCS_STATUS_RESULT == ok-sync ]]; then
    local DIRTY=0
    if [[ $VCS_STATUS_NUM_UNSTAGED -gt 0 ]] || [[ $VCS_STATUS_NUM_UNTRACKED -gt 0 ]]; then
      DIRTY=1
    fi

    if [[ $DIRTY -eq 1 ]]; then
    git_status="%F{cyan}$VCS_STATUS_LOCAL_BRANCH%f %F{yellow}▲%f "
    else
      git_status="%F{cyan}$VCS_STATUS_LOCAL_BRANCH%f %F{green}▲%f "
    fi
  fi
}

if [[ $CONF_ENABLEGIT -eq 1 ]]; then
  precmd_functions+=(git_status_v2)
  gitstatus_stop 'PROMPT' && gitstatus_start -s -1 -u -1 -c -1 -d -1 'PROMPT'
fi

timer=0
timer_show=0
preexec_time() {
  timer=${timer:-$SECONDS}
}
precmd_time() {
  if [ $timer ]; then
    timer_show=$(($SECONDS-$timer))

    unset timer
  fi
}
preexec_functions+=(preexec_time)
precmd_functions+=(precmd_time)

prompt_time() {
  local out=""

  if [ $timer_show -lt 3 ]; then
    timer_display=$out
    return
  fi

  if [ $timer_show -ge 3600 ]; then
    out+="$(printf '%d' $(($timer_show/3600)))h"
  fi

  if [ $timer_show -ge 60 ]; then
    out+="$(printf '%d' $(($timer_show%3600/60)))m"
  fi

  out+="$(printf '%d' $(($timer_show%60)))s"

  timer_display=$out
}
precmd_functions+=(prompt_time)

prompt_clock=""
binary_clock() {
  local row0=("⠀" "⠈" "⠐" "⠘" "⠠" "⠨" "⠰" "⠸" "⢀" "⢈")
  local row1=("⠁" "⠉" "⠑" "⠙" "⠡" "⠩" "⠱" "⠹" "⢁" "⢉")
  local row2=("⠂" "⠊" "⠒" "⠚" "⠢" "⠪" "⠲" "⠺" "⢂" "⢊")
  local row3=("⠃" "⠋" "⠓" "⠛" "⠣" "⠫" "⠳" "⠻" "⢃" "⢋")
  local row4=("⠄" "⠌" "⠔" "⠜" "⠤" "⠬" "⠴" "⠼" "⢄" "⢌")
  local row5=("⠅" "⠍" "⠕" "⠝" "⠥" "⠭" "⠵" "⠽" "⢅" "⢍")
  local chars=("${row0[@]}" "${row1[@]}" "${row2[@]}" "${row3[@]}" "${row4[@]}" "${row5[@]}")

  local hour=$(date +"%H")
  local minute=$(date +"%M")
  local second=$(date +"%S")
  [[ $hour = 0* ]] && hour=${hour:1:1}
  [[ $minute = 0* ]] && minute=${minute:1:1}
  [[ $second = 0* ]] && second=${second:1:1}

  prompt_clock="${chars[$hour+1]} ${chars[$minute+1]} ${chars[$second+1]}"
}

normal_clock() {
  prompt_clock=$(date +"%H:%M:%S")
}

if [[ $CONF_BINARYCLOCK -eq 1 ]]; then
  precmd_functions+=(binary_clock)
else
  precmd_functions+=(normal_clock)
fi

ssh_hostname=""
get_is_in_ssh() {
  if [[ ! -z $SSH_TTY ]]; then
    ssh_hostname="%F{blue}[%m]%f "
  elif [[ ! -z $WSL_DISTRO_NAME ]]; then
    ssh_hostname="%F{blue}[$WSL_DISTRO_NAME]%f "
  fi
}
precmd_functions+=(get_is_in_ssh)

PROMPT='$ssh_hostname %(?.%F{magenta}.%F{red})λ%f  %1~ $git_status '
RPROMPT='%F{yellow}$timer_display%f  %(?..%F{red}%?%f  )%F{white}$prompt_clock%f'

####

# keys
autoload -Uz up-line-or-beginning-search
autoload -Uz down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

bindkey '^?'      backward-delete-char          # bs         delete one char backward
bindkey '^[[3~'   delete-char                   # delete     delete one char forward
bindkey '^[[P'    delete-char                   # delete (2) delete one char forward
bindkey '^[[H'    beginning-of-line             # home       go to the beginning of line
bindkey '^[[F'    end-of-line                   # end        go to the end of line
bindkey '^[[1~'   beginning-of-line             # home (2)   go to the beginning of line
bindkey '^[[4~'   end-of-line                   # end (2)    go to the end of line
bindkey '^[[1;5C' forward-word                  # ctrl+right go forward one word
bindkey '^[[1;5D' backward-word                 # ctrl+left  go backward one word
bindkey '^H'      backward-kill-word            # ctrl+bs    delete previous word
bindkey '^[[3;5~' kill-word                     # ctrl+del   delete next word
bindkey '^J'      backward-kill-line            # ctrl+j     delete everything before cursor
bindkey '^[[D'    backward-char                 # left       move cursor one char backward
bindkey '^[[C'    forward-char                  # right      move cursor one char forward
bindkey '^[[A'    up-line-or-beginning-search   # up         prev command in history
bindkey '^[[B'    down-line-or-beginning-search # down       next command in history
bindkey '^[[5~'   history-search-backward       # pgup       prev in history no search
bindkey '^[[6~'   history-search-forward        # pgdn       next in history no search
bindkey '^F'      expand-or-complete            # ctrl+f     tab

[[ $CONF_ENABLEAC -eq 1 ]] && bindkey '^G' autosuggest-accept

bindkey -r '^I'

####

# aliases
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

alias cls='clear'
alias del='rm'

if [[ -x "$(command -v eza)" ]]; then
  alias ls='eza'
fi
alias ll='ls -lah'
alias la='ls -a'

####

# functions
command_not_found_handler() {
    echo "'$1' is not recognized as an internal or external command,"
    echo "operable program or batch file."
    return 127
}

extract () {
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1          ;;
          *.tar.gz)    tar xvzf $1          ;;
          *.bz2)       bunzip2 $1           ;;
          *.rar)       unrar x $1           ;;
          *.gz)        gunzip $1            ;;
          *.tar)       tar xvf $1           ;;
          *.tbz2)      tar xvjf $1          ;;
          *.tgz)       tar xvzf $1          ;;
          *.zip)       unzip $1             ;;
          *.Z)         uncompress $1        ;;
          *.7z)        7z x $1              ;;
          *.asar)      asar e $1 ${1%.asar} ;;
          *)           echo "'$1' cannot be extracted via >extract<" ;;
      esac
  else
      echo "'$1' is not a valid file!"
  fi
}

remindperms () {
  echo "               0    (No read, no write, no execute)  ---"
  echo "               1    (No read, no write, execute)     --x"
  echo "               2    (No read, write, no execute)     -w-"
  echo "               3    (No read, write, execute)        -wx"
  echo "               4    (Read, no write, no execute)     r--"
  echo "               5    (Read, no write, execute)        r-x"
  echo "               6    (Read, write, no execute)        rw-"
  echo "               7    (Read, write, execute)           rwx"
  echo ""
  echo "(who)          u    User"
  echo "(who)          g    Group owner"
  echo "(who)          o    Other"
  echo "(who)          a    All (“world”)"
  echo ""
  echo "(action)       +    Adding permissions"
  echo "(action)       -    Removing permissions"
  echo "(action)       =    Explicitly set permissions"
  echo ""
  echo "(permissions)  r    Read"
  echo "(permissions)  w    Write"
  echo "(permissions)  x    Execute"
  echo "(permissions)  t    Sticky bit"
  echo "(permissions)  s    Set UID or GID"
}

####

if [[ -f "$HOME/.zshenv" ]]; then
  source "$HOME/.zshenv"
fi

if [[ $CONF_FORTUNE -eq 1 ]]; then
  fortune $CONF_FORTUNE_PATH
  echo
fi

[[ $CONF_ENABLESH -eq 1 ]] && source "$HOME/.zsh/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh"
