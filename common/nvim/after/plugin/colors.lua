vim.o.termguicolors = 1
vim.g.mode = "focus"
vim.cmd("colorscheme amora")

local function c(name)
  return vim.g["amora#palette"][name]
end

local function h(scope, fg, bg, attr_list, special)
  bg = bg or {"NONE", "NONE"}
  attr_list = attr_list or {"NONE"}
  local attrs = table.concat(attr_list, ",")
  special = special or fg and fg or {"NONE", "NONE"}

  local cmd = "highlight %s "
  if fg then
    cmd = cmd .. "guifg=%s ctermfg=%s "
  end
  cmd = cmd .. "guibg=%s ctermbg=%s "
  cmd = cmd .. "gui=%s cterm=%s "
  cmd = cmd .. "guisp=%s"

  if fg then
    cmd = cmd:format(scope, fg[1], fg[2], bg[1], bg[2], attrs, attrs, special[1])
  else
    cmd = cmd:format(scope, bg[1], bg[2], attrs, attrs, special[1])
  end

  vim.cmd(cmd)
end

vim.cmd("hi! link IndentBlanklineIndent1 AmoraYellow")
vim.cmd("hi! link IndentBlanklineIndent2 AmoraGreen")
vim.cmd("hi! link IndentBlanklineIndent3 AmoraPink")
vim.cmd("hi! link IndentBlanklineIndent4 AmoraPurple")

vim.cmd("hi! link IndentBlanklineChar NonText")
vim.cmd("hi! link IndentBlanklineSpaceChar NonText")
vim.cmd("hi! link IndentBlanklineSpaceCharBlankline NonText")
vim.cmd("hi! clear IndentBlanklineContextStart")
h("IndentBlanklineContextChar", c("selection"))

vim.cmd("hi! link SignColumn LineNr")

vim.cmd("hi! link Hlargs AmoraYellowItalic")
vim.cmd("hi! link Function AmoraCyan")
vim.cmd("hi! link Statement AmoraRed")
vim.cmd("hi! link Conditional AmoraRed")
vim.cmd("hi! link Repeat AmoraRed")
vim.cmd("hi! link Label AmoraRed")
vim.cmd("hi! link Keyword AmoraRed")
vim.cmd("hi! link Exception AmoraRed")
vim.cmd("hi! link Operator AmoraRed")
vim.cmd("hi! link javaScriptRegexpString AmoraYellow")

vim.cmd("hi! link CmpItemKindDefault AmoraPink")
vim.cmd("hi! link LspDiagnosticsDefaultWarning AmoraYellow")
vim.cmd("hi! link DiagnosticHint AmoraCyan")
vim.cmd("hi! link DiagnosticError AmoraRed")
vim.cmd("hi! link DiagnosticWarn AmoraYellow")
vim.cmd("hi! link DiagnosticInfo AmoraFg")

h("Normal", c("fg"), c("bg"))
h("NonText", c("bglighter"))
h("IncSearch", c("bg"), c("yellow"))
h("WarningMsg", c("bg"), c("yellow"))
h("ErrorMsg", c("fg"), c("red"))
h("ExtraWhitespace", nil, c("red"))
h("ColorColumn", c("yellow"), c("bglighter"))
h("MatchParen", c("selection"), nil, {"underline"})

h("BufferCurrent", c("pink"))
h("BufferCurrentIndex", c("pink"))
h("BufferCurrentMod", c("yellow"))
h("BufferCurrentSign", c("pink"))
h("BufferCurrentTarget", c("pink"))
h("BufferVisible", c("fg"))
h("BufferVisibleIndex", c("fg"))
h("BufferVisibleMod", c("selection"))
h("BufferVisibleSign", c("fg"))
h("BufferVisibleTarget", c("fg"))
h("BufferInactive", c("comment"), c("bgdark"))
h("BufferInactiveIndex", c("pink"), c("bgdark"))
h("BufferInactiveMod", c("comment"), c("bgdark"))
h("BufferInactiveSign", c("comment"), c("bgdark"))
h("BufferInactiveTarget", c("green"), c("bgdark"))
h("BufferTabpages", c("fg"), c("bgdark"))
h("BufferTabpageFill", c("fg"), c("bgdark"))
h("BufferOffset", c("bg"), c("bg"))

h("WilderAccent", c("pink"))
h("WilderSelectedAccent", c("fg"), c("selection"))

--[[vim.cmd("colorscheme lena")

vim.cmd("highlight IndentBlanklineIndent1 ctermfg=11 cterm=nocombine")
vim.cmd("highlight IndentBlanklineIndent2 ctermfg=10 cterm=nocombine")
vim.cmd("highlight IndentBlanklineIndent3 ctermfg=13 cterm=nocombine")
vim.cmd("highlight IndentBlanklineIndent4 ctermfg=12 cterm=nocombine")

vim.cmd("hi Normal ctermbg=NONE")
vim.cmd("hi LineNr ctermfg=7 ctermbg=NONE")
vim.cmd("hi CursorLineNr ctermfg=1 ctermbg=0")
vim.cmd("hi CursorLine cterm=NONE ctermfg=NONE ctermbg=0")
vim.cmd("hi StatusLine ctermfg=5 ctermbg=NONE cterm=NONE")
vim.cmd("hi StatusLineNC ctermfg=7 ctermbg=NONE cterm=NONE")
vim.cmd("hi VertSplit ctermbg=0 ctermfg=0")
vim.cmd("hi ExtraWhitespace ctermbg=1")
vim.cmd("hi ColorColumn ctermbg=8 ctermfg=3")

vim.cmd("hi BufferCurrent ctermfg=5 ctermbg=NONE")
vim.cmd("hi BufferCurrentIndex ctermfg=5 ctermbg=NONE")
vim.cmd("hi BufferCurrentMod ctermfg=9 ctermbg=NONE")
vim.cmd("hi BufferCurrentSign ctermfg=5 ctermbg=NONE")
vim.cmd("hi BufferCurrentTarget ctermfg=5 ctermbg=NONE")
vim.cmd("hi BufferVisible ctermfg=7 ctermbg=NONE")
vim.cmd("hi BufferVisibleIndex ctermfg=7 ctermbg=NONE")
vim.cmd("hi BufferVisibleMod ctermfg=9 ctermbg=NONE")
vim.cmd("hi BufferVisibleSign ctermfg=7 ctermbg=NONE")
vim.cmd("hi BufferVisibleTarget ctermfg=7 ctermbg=NONE")
vim.cmd("hi BufferInactive ctermfg=7 ctermbg=0")
vim.cmd("hi BufferInactiveIndex ctermfg=5 ctermbg=0")
vim.cmd("hi BufferInactiveMod ctermfg=1 ctermbg=0")
vim.cmd("hi BufferInactiveSign ctermfg=10 ctermbg=0")
vim.cmd("hi BufferInactiveTarget ctermfg=11 ctermbg=0")
vim.cmd("hi BufferTabpages ctermfg=7 ctermbg=0")
vim.cmd("hi BufferTabpageFill ctermfg=7 ctermbg=0")
vim.cmd("hi BufferOffset ctermfg=0 ctermbg=0")

vim.cmd("hi! link SignColumn LineNr")

vim.cmd("hi DiagnosticHint ctermfg=6")
vim.cmd("hi DiagnosticError ctermfg=1")
vim.cmd("hi DiagnosticWarn ctermfg=3")
vim.cmd("hi DiagnosticInfo ctermfg=7")--]]
