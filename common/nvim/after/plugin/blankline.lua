require("indent_blankline").setup({
  char = "▎",
  space_char_blankline = " ",
  char_blankline = " ",
  char_highlight_list = {
    "IndentBlanklineIndent1",
    "IndentBlanklineIndent2",
    "IndentBlanklineIndent3",
    "IndentBlanklineIndent4",
  },
  show_current_context = true,
  show_current_context_start = true,
})

