local telescope = require("telescope")
telescope.setup({
  defaults = {
    vimgrep_arguments = {"ag", "--vimgrep"},
  }
})

local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", builtin.find_files)
vim.keymap.set("n", "<leader>fg", builtin.git_files)
vim.keymap.set("n", "<leader>fs", builtin.live_grep)
