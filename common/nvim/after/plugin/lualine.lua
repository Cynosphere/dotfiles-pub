--[[local theme = require("lualine.themes.16color")
theme.normal.a.bg = 7
theme.visual.a.bg = 4
theme.command = {
  a = {bg = 6},
}
theme.terminal = {
  a = {bg = 5},
}
theme.normal.c.bg = "none"--]]

local function c(name)
  return vim.g["amora#palette"][name][1]
end

local theme = {
  normal = {
    a = {
      fg = c("bg"),
      bg = c("fg"),
    },
    b = {
      fg = c("fg"),
      bg = c("bglighter"),
    },
    c = {
      fg = c("fg"),
      bg = c("bg"),
    },
  },
  insert = {
    a = {
      fg = c("bg"),
      bg = c("green"),
    },
  },
  visual = {
    a = {
      fg = c("bg"),
      bg = c("purple"),
    },
  },
  replace = {
    a = {
      fg = c("bg"),
      bg = c("red"),
    },
  },
  command = {
    a = {
      fg = c("bg"),
      bg = c("cyan"),
    },
  },
  terminal = {
    a = {
      fg = c("bg"),
      bg = c("pink"),
    },
  },
  inactive = {
    a = {
      fg = c("fg"),
      bg = c("bgdarker"),
    },
    b = {
      fg = c("fg"),
      bg = c("bgdarker"),
    },
    c = {
      fg = c("fg"),
      bg = c("bgdarker"),
    },
  },
}
require("lualine").setup({
  options = {
    icons_enabled = false,
    theme = theme,
    section_separators = "",
    component_separators = "",
    disabled_filetypes = {
      statusline = {"NvimTree", "undotree", "diff", "tsplayground", "packer"},
    },
  },
  sections = {
    lualine_a = {
      {
        "mode",
        fmt = function(str)
          return str:sub(1,3)
        end
      },
    },
    lualine_b = {
      {
        "branch",
        color = {
          bg = c("pink"),
          fg = c("bgdarker"),
        },
      },
      {
        "diff",
        diff_color = {
          added = {
            bg = c("bgdark"),
            fg = c("green"),
          },
          modified = {
            bg = c("bgdark"),
            fg = c("yellow"),
          },
          removed = {
            bg = c("bgdark"),
            fg = c("red"),
          },
        },
      },
      {
        "diagnostics",
        sections = {"error", "warn", "info", "hint"},
        diagnostics_color = {
          error = {
            bg = c("bgdark"),
            fg = c("red"),
          },
          warn = {
            bg = c("bgdark"),
            fg = c("yellow"),
          },
          info = {
            bg = c("bgdark"),
            fg = c("fg"),
          },
          hint = {
            bg = c("bgdark"),
            fg = c("cyan"),
          },
        },
      },
    },
    lualine_c = {
      {
        "filename",
        color = {
          bg = c("bg"),
          fg = c("pink"),
        },
      },
    },
    lualine_x = {
      {
        "location",
        color = {
          bg = c("purple"),
          fg = c("bg"),
        },
      },
    },
    lualine_y = {
      {
        "encoding",
         color = {
          bg = c("yellow"),
          fg = c("bg"),
        },
        padding = {
          left = 1,
          right = 0,
        },
      },
      {
        "fileformat",
        color = {
          bg = c("yellow"),
          fg = c("bg"),
        },
      },
    },
    lualine_z = {
      {
        "filetype",
        color = {
          bg = c("fg"),
          fg = c("bg"),
        },
      },
    },
  },
})
