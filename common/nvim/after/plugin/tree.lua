require("nvim-tree").setup({
  actions = {
    open_file = {
      quit_on_open = true,
    },
  },
  renderer = {
    indent_markers = {
      enable = true,
    },
    icons = {
      git_placement = "after",
      symlink_arrow = " -> ",
      show = {
        file = false,
        folder = false,
      },
      glyphs = {
        folder = {
          arrow_closed = "+",
          arrow_open = "-",
        },
        git = {
          unstaged = "U",
          staged = "S",
          unmerged = "M",
          renamed = "R",
          untracked = "A",
          deleted = "D",
          ignored = "I",
        },
      },
    },
  },
})

vim.keymap.set("n", "<leader>b", vim.cmd.NvimTreeToggle)
