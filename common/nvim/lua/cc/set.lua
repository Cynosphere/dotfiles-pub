vim.opt.termguicolors = false

vim.opt.fileformat = "unix"
vim.opt.fileformats = {"unix", "dos"}

vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.cul = true

vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.list = true
vim.opt.listchars:append("space:⋅")
vim.opt.listchars:append("eol:↴")
vim.opt.fillchars:append("vert:▎")

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"

vim.g.ackprg = "ag --vimgrep"

