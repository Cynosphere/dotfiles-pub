vim.cmd("packadd packer.nvim")

local packer = require("packer")

return packer.startup(function(use)
  use("wbthomason/packer.nvim")

  -- {{{ color scheme
  use("owozsh/amora")
  -- }}}

  -- {{{ treesitter
  use(
    "nvim-treesitter/nvim-treesitter",
    {run = ":TSUpdate"}
  )
  use("nvim-treesitter/playground")
  use("nvim-treesitter/nvim-treesitter-context")
  use("windwp/nvim-ts-autotag")
  use("m-demare/hlargs.nvim")
  -- }}}

  -- {{{ lsp
  use({
    "VonHeikemen/lsp-zero.nvim",
    branch = "v2.x",
    requires = {
      -- LSP Support
      {"neovim/nvim-lspconfig"},             -- Required
      {                                      -- Optional
        "williamboman/mason.nvim",
        run = function()
          pcall(vim.cmd, "MasonUpdate")
        end,
      },
      {"williamboman/mason-lspconfig.nvim"}, -- Optional

      -- Autocompletion
      {"hrsh7th/nvim-cmp"},     -- Required
      {"hrsh7th/cmp-nvim-lsp"}, -- Required
      {"L3MON4D3/LuaSnip"},     -- Required
    },
  })
  use("jose-elias-alvarez/null-ls.nvim")
  use("MunifTanjim/prettier.nvim")
  --use("ray-x/lsp_signature.nvim")
  -- }}}

  -- {{{ misc
  use({
    "tpope/vim-fugitive",
    config = function()
      vim.keymap.set("n", "<leader>gs", vim.cmd.Git)
    end
  })
  use("lukas-reineke/indent-blankline.nvim")
  use({
    "ntpeters/vim-better-whitespace",
    config = function()
      vim.g.better_whitespace_enable = true
      vim.g.strip_whitespace_on_save = true
    end
  })
  use({
    "andweeb/presence.nvim",
    config = function()
      vim.g.presence_buttons = false
    end
  })
  use({
    "NvChad/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup()
    end
  })
  --[[use({
    "yamatsum/nvim-cursorline",
    config = function()
      require("nvim-cursorline").setup({
        cursorline = {
          enable = false,
        },
        cursorword = {
          enable = true,
        },
      })
    end
  })--]]
  use({
    "windwp/nvim-autopairs",
    config = function()
      require("nvim-autopairs").setup()
    end
  })
  use("jghauser/mkdir.nvim")
  use({
    "kylechui/nvim-surround",
    config = function()
      require("nvim-surround").setup()
    end
  })
  -- }}}

  -- {{{ ui
  use({
    "nvim-telescope/telescope.nvim",
    tag = "0.1.1",
    requires = {
      {"nvim-lua/plenary.nvim"},
    },
  })
  use({
    "mbbill/undotree",
    config = function()
      vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)
    end
  })
  use("romgrk/barbar.nvim")
  use("nvim-tree/nvim-tree.lua")
  use("nvim-lualine/lualine.nvim")
  use({
    "folke/which-key.nvim",
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
      require("which-key").setup()
    end
  })
  -- }}}

  -- this is only here to shut up the message on startup
  require("barbar").setup({
    icons = {
      filetype = {
        enabled = false,
      },
    },
  })
end)
