vim.g.mapleader = " "

vim.keymap.set("t", "<Esc>", "<C-\\><C-n>")

vim.keymap.set("i", "<C-f>", "<C-i>", {silent = true})
vim.keymap.set("i", "<C-g>", "<C-d>", {silent = true})
vim.keymap.set({"i", "v"}, "<Tab>", "<Esc>", {remap = true})

vim.keymap.set("v", "<S-Down>", ":m '>+1<CR>gv=gv", {silent = true})
vim.keymap.set("v", "<S-Up>", ":m '<-2<CR>gv=gv", {silent = true})

vim.keymap.set("i", "<S-Down>", "<Esc>:m+<CR>", {silent = true})
vim.keymap.set("i", "<S-Up>", "<Esc>:m-2<CR>", {silent = true})

vim.keymap.set("n", "<S-Down>", ":m+<CR>", {silent = true})
vim.keymap.set("n", "<S-Up>", ":m-2<CR>", {silent = true})

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("x", "<leader>p", "\"_dP")
vim.keymap.set({"n", "v"}, "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")
vim.keymap.set({"n", "v"}, "<leader>d", "\"_d")

vim.keymap.set("n", "Q", "<nop>")

vim.keymap.set("n", "<leader>s", [[:s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("n", "<leader>S", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

vim.keymap.set("n", "<leader><leader>", function()
    vim.cmd("so")
end)
vim.keymap.set("n", "<leader>vpp", function()
  local dir = vim.call("stdpath", "config")
  vim.cmd("e " .. dir .. "/lua/cc/packer.lua")
end)
vim.keymap.set("n", "<leader>vps", "<cmd>PackerSync<CR>")
