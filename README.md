# dotfiles but public

## Current Setup
OS: Windows 10 Home 22H2

Visual Style: [XMC](https://github.com/niivu/Windows-10-themes/tree/main/xmc%202) + Classic Theme

"Window Manager": [ct"wm" (bug.n fork)](https://gitdab.com/Cynosphere/bug.n)

Terminal: wezterm

Shell: PowerShell 7

Music Player: MusicBee + AudioBand for taskbar display

Text Editor: nano + VSCode

Browser: Firefox with [MSFX fork](https://gitdab.com/Cynosphere/MSFX)

<details>
  <summary><h3>Firefox Extensions and Userscripts</h3></summary>

#### Extensions
##### General Use
* Dark Reader
* Decentraleyes
* FastForward
* Image Search Options
* LibRedirect
* Multithreaded Download Manager
* Neat URL
* NoScript
* Privacy Pass
* Stylus
* Tabliss
* uBlock Origin
* User-Agent Switcher and Manager
* Violentmonkey

##### Site Specific
* Alternate Player for Twitch.tv
* AugmentedSteam
* Disable Google Lens
* FrankerFaceZ
* Old Reddit Redirect
* Reddit Enhancement Suite
* SponsorBlock
* SteamDB
* Stop AutoPlay Next for YouTube
* Truffle.TV

##### Niche
* OverbiteWX
* React Developer Tools
* Right-Click Borescope
* ShareX
* To Discord
* Which Cloudflare datacenter am I visiting?

##### Legacy (requires firefox-scripts extensions support)
* Save File to
#### Userscripts
##### Browser
* Auto Plain Text Links
* Extension Options Menu
* PrivateTab
* Status Bar

##### Violentmonkey
* Anti-Adblock Killer
* AdsBypasser
* [IMGXIS](https://github.com/Cynosphere/IMGXIS)
* AntiAdware
* Return YouTube Dislike
* `@kawai-scripts/soundcloud-downloader`

a bunch of others that I've written or are publicly incriminating :^)
</details>

### Classic Theme
* [OpenShell](https://github.com/Open-Shell/Open-Shell-Menu) with [valinet's classic theme patch](https://github.com/valinet/ExplorerPatcher/discussions/167#discussioncomment-1517997)
* Folder Options X
* [Command Bar shellstyle mod](https://www.askvg.com/how-to-make-folder-band-auto-hidden-in-windows-vista/)
* Windhawk mods:
  * [Classic Theme Explorer](https://gitdab.com/Cynosphere/WindhawkMods/src/branch/main/classic-theme-explorer.wh.cpp)
    * Replaces [SCT.FEH](https://github.com/AEAEAEAE4343/SimpleClassicTheme.FileExplorerHook) and [ExplorerPatcher](https://github.com/valinet/ExplorerPatcher)
  * [Classic Theme Windows](https://gitdab.com/Cynosphere/WindhawkMods/src/branch/main/classic-theme-windows.wh.cpp)
    * Replaces SimpleClassicTray, [BasicThemer2](https://github.com/Ingan121/BasicThemer2) and [Classic File Picker](https://gitdab.com/Cynosphere/WindhawkMods/src/branch/main/classic-file-picker.wh.cpp)
  * Windows 7 Command Bar
  * Disable Immersive Context Menus
  * Non Immersive Taskbar Context Menu
    * Disable icons if Explorer windows refuse to load.
* Toolbars:
  * OpenShell's Classic Explorer Bar
  * [Classic Address Bar](https://winclassic.net/thread/1004/classic-toolbars-address-explorer-restored)
  * QtTabBar (currently not in use)
    * Uninstalling QtTabBar **will remove all your other toolbar entries**.
* [Windows Classic 7tsp Icon Pack](https://winclassic.net/thread/1152/windows-classic-icon-pack-7tsp)

### Windhawk Mods (non-classic)
* Taskbar Clock Customization

### Other Misc Programs
* ShareX
* EarTrumpet
* AltSnap
* Everything
* Wox
* WinCompose
* PowerToys
* HWiNFO64 - Systray temps for CPU and GPU
* 7+ Taskbar Tweaker [(settings)](windows/7tt.png)
